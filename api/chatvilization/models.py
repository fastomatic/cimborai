from django.db import models

from chatengine.models import ChatBot


class Army(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    commander = models.ForeignKey(
        "chatengine.ChatUser", on_delete=models.CASCADE, related_name="commander"
    )
    size = models.IntegerField(default=0)
    attacking_user = models.ForeignKey(
        "chatengine.ChatUser",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="attacking_user",
    )
    attack_started_at = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    context = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.commander} ({self.size})"


class MessageQueue(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_displayed = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.chatbot}: {self.message}"


class ArmyOffers(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    offerer = models.ForeignKey("chatengine.ChatUser", on_delete=models.CASCADE, related_name="offerer")
    quantity = models.IntegerField(default=0)
    price = models.FloatField(default=1.0)
    offer_for = models.ForeignKey("chatengine.ChatUser", on_delete=models.CASCADE, blank=True, null=True, related_name="offer_for")

class ArmyPriceHistory(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    price = models.FloatField(default=1.0)
    time = models.DateTimeField(auto_now_add=True)
    