from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import Army, ArmyOffers, MessageQueue, ArmyPriceHistory

class ArmyAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "commander", "size", "is_deleted", "attacking_user", "attack_started_at")
    list_filter = ("is_deleted", "chatbot", "commander", "attacking_user")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(is_deleted=False, chatbot__owner_user=request.user)

    class Meta:
        model = Army

class ArmyOffersAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "offerer", "quantity", "price", "offer_for")
    list_filter = ("chatbot", "offerer", "offer_for")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(chatbot__owner_user=request.user)

    class Meta:
        model = ArmyOffers

class MessageQueueAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "message", "created_at", "is_displayed")
    list_filter = ("chatbot", "is_displayed")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(chatbot__owner_user=request.user)

    class Meta:
        model = MessageQueue

class ArmyPriceHistoryAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "price", "time")
    list_filter = ("chatbot", "price")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(chatbot__owner_user=request.user)

    class Meta:
        model = ArmyPriceHistory


admin.site.register(Army, ArmyAdmin)
admin.site.register(ArmyOffers, ArmyOffersAdmin)
admin.site.register(MessageQueue, MessageQueueAdmin)
admin.site.register(ArmyPriceHistory, ArmyPriceHistoryAdmin)
