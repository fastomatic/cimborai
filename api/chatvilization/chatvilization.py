import datetime
import random
from asgiref.sync import sync_to_async

from chatvilization.models import Army, MessageQueue, ArmyOffers, ArmyPriceHistory
from chatengine.models import ChatUser
from chatengine.scheduler import get_scheduler
from django.utils.timezone import now


class ChatVilization:
    def __init__(self, chatbot, platform):
        self.soldier_price = 100.0
        self.last_check = datetime.datetime.now()
        self.last_price_change = datetime.datetime.now()
        self.chatbot = chatbot
        self.platform = platform
        self.initialize_game()
        get_scheduler().add_task(self.check_expiring_tasks)

    # This runs in a different process, not in the chatbot itself!
    async def check_expiring_tasks(
        self, log, platform
    ):  # pylint: disable=unused-argument
        return await self.sync_check_expiring_tasks(log, platform)

    def wipe_armies(self):
        Army.objects.filter(chatbot=self.chatbot).delete()
        ArmyOffers.objects.filter(chatbot=self.chatbot).delete()
        ArmyPriceHistory.objects.filter(chatbot=self.chatbot).delete()

    @sync_to_async
    def sync_check_expiring_tasks(
        self, log, platform
    ):  # pylint: disable=unused-argument
        if not self.chatbot.chatvilization_enabled:
            return []
        if self.chatbot.chatvilization_dconly and platform != "discord":
            return []
        if datetime.datetime.now() - self.last_check < datetime.timedelta(seconds=60):
            return []
        log_context = ""
        message = ""
        if datetime.datetime.now() - self.last_price_change > datetime.timedelta(
            minutes=self.chatbot.chatvilization_price_change_interval
        ):
            self.initialize_game()
            self.last_price_change = datetime.datetime.now()
            log_context += f"Price of soldier changed to {self.soldier_price}\n"
            message += f"A katonák ára megváltozott, most {self.soldier_price} zsetonba kerülnek!\n"
        self.last_check = datetime.datetime.now()
        print(f"Checking expiring tasks at {datetime.datetime.now()}")
        armies = Army.objects.filter(
            attack_started_at__isnull=False,
            attacking_user__isnull=False,
            is_deleted=False,
        )
        curr_soldier_price = self.soldier_price
        for army in armies:
            if army.attack_started_at:
                # datetime.now() is timezone aware, but attack_started_at is not
                time_since_attack = now() - army.attack_started_at
                if time_since_attack.total_seconds() > 60:
                    home_army_of_opponent = self.get_home_army(army.attacking_user)
                    log_context += f"Army of {army.commander.visible_name} (size {army.size}) has arrived at {army.attacking_user.visible_name} who has {home_army_of_opponent.size} defending!\n"
                    message += f"Kedves @{army.attacking_user.visible_name}, {army.size} méretű sereg érkezett @{army.commander.visible_name} felhasználótól! A Te védekező sereged {home_army_of_opponent.size} fős. "
                    if home_army_of_opponent.size <= army.size:
                        bonus_multiplier = 1
                        if home_army_of_opponent.size == army.size:
                            bonus_multiplier = 1
                            log_context += (
                                "Armies are equal in size, no bonus multiplier\n"
                            )
                        else:
                            log_context += f"Army of {army.commander.visible_name} is bigger, bonus multiplier is 2\n"
                        beaten_army_size = home_army_of_opponent.size
                        surviving_army_size = army.size - beaten_army_size
                        home_army_of_opponent.size = 0
                        home_army_of_opponent.save()
                        points_lost = min(
                            surviving_army_size * curr_soldier_price,
                            army.attacking_user.current_score,
                        )
                        army.attacking_user.current_score -= points_lost
                        army.attacking_user.save()
                        log_context += f"Attacked user lost {points_lost} points, now has {army.attacking_user.current_score} points.\n"
                        message += f"Vesztettél {points_lost} zsetont, most {army.attacking_user.current_score} zsetonod van! "
                        army.commander.current_score += points_lost * bonus_multiplier
                        army.commander.save()
                        log_context += f"Attacking user won {points_lost * bonus_multiplier} points, now has {army.commander.current_score} points.\n"
                        message += f"Kedves @{army.commander.visible_name}, nyertél {points_lost * bonus_multiplier} zsetont, most {army.commander.current_score} zsetonod van! "
                    else:
                        home_army_of_opponent.size -= army.size
                        home_army_of_opponent.save()
                        log_context += f"Attacked user still has {home_army_of_opponent.size} soldiers left.\n"
                        message += f"Vesztettél {army.size} katonát! "
                    army.is_deleted = True
                    army.context = log_context
                    army.save()
                    _ = MessageQueue.objects.create(
                        chatbot=army.chatbot, message=message
                    )
        log.context += log_context
        return message

    def initialize_game(self):
        # we need to generate a random float percent which can be either negative or positive, and is between self.chatbot.chatvilization_min_percent and self.chatbot.chatvilization_max_percent
        change_percent = random.uniform(self.chatbot.chatvilization_min_percent, self.chatbot.chatvilization_max_percent)
        change_percent_with_sign = change_percent if random.random() < 0.5 else -change_percent
        new_price = self.soldier_price * (1 + change_percent_with_sign / 100)
        if new_price < self.chatbot.chatvilization_min_price:
            new_price = self.chatbot.chatvilization_min_price
        if new_price > self.chatbot.chatvilization_max_price:
            new_price = self.chatbot.chatvilization_max_price
        self.soldier_price = round(new_price, self.chatbot.chatvilization_rounding_precision)
        history = ArmyPriceHistory.objects.create(chatbot=self.chatbot, price=self.soldier_price)
        history.save()

    @sync_to_async
    def get_armies_status(self, chatuser):
        qs = Army.objects.filter(
            chatbot=self.chatbot, commander=chatuser, is_deleted=False
        ).prefetch_related("attacking_user")
        retval = list(qs)
        return retval

    def get_home_army(self, chatuser):
        qs = Army.objects.filter(
            chatbot=self.chatbot,
            commander=chatuser,
            attacking_user=None,
            is_deleted=False,
        )
        if qs.exists():
            return qs.first()
        return None

    @sync_to_async
    def get_attackable_chatuser(self, attacked_username, attacking_user):
        if not ChatUser.objects.filter(
            username=attacked_username, platform=self.platform
        ).exists():
            return f"nincs ilyen felhasználó: {attacked_username}"
        chatuser = ChatUser.objects.get(
            username=attacked_username, platform=self.platform
        )
        defending_army = Army.objects.filter(
            chatbot=self.chatbot,
            commander=chatuser,
            attacking_user=None,
            is_deleted=False,
        ).first()
        if not defending_army:
            return f"{attacked_username} felhasználónak nincs védekező serege, így nem vesz részt a játékban!"
        if defending_army.size < 1:
            attacking_armies = Army.objects.filter(
                chatbot=self.chatbot,
                commander=chatuser,
                attacking_user__isnull=False,
                is_deleted=False,
            )
            if not attacking_armies.exists():
                return f"{attacked_username} pillanatnyilag harcképtelen, így nem vesz részt a játékban!"
        # Has chatuser attacked username 3 times this day?
        today_attacks = Army.objects.filter(
            chatbot=self.chatbot,
            commander=attacking_user,
            attacking_user=chatuser,
            attack_started_at__date=now().date(),
        )
        if today_attacks.count() >= 3:
            return f"ma már háromszor küldtél sereget {attacked_username} felhasználó ellen ma, próbáld újra holnap!"
        return chatuser

    @sync_to_async
    def attack(self, chatuser, attacking_user, army_size):
        if army_size < 1:
            return f"Kedves {chatuser.visible_name}, a támadó sereg méretének pozitívnak kell lennie!"
        home_army = self.get_home_army(chatuser)
        if home_army.size < army_size:
            return f"Kedves {chatuser.visible_name}, nincs elég katonád, hogy {army_size} méretű sereggel támadj!"
        attacking_qs = Army.objects.filter(
            chatbot=self.chatbot,
            commander=chatuser,
            attacking_user=attacking_user,
            is_deleted=False,
        )
        if attacking_qs.exists():
            return f"Kedves {chatuser.visible_name}, már támadod {attacking_user.visible_name} seregét!"
        home_army.size -= army_size
        home_army.save()
        _ = Army.objects.create(
            chatbot=self.chatbot,
            commander=chatuser,
            attacking_user=attacking_user,
            size=army_size,
        )
        return f"Kedves {chatuser.visible_name}, {army_size} méretű sereggel támadod {attacking_user.visible_name} seregét! A sereg 1 perc múlva érkezik!"

    @sync_to_async
    def buy_soldiers(self, chatuser, quantity):
        if quantity < 1:
            return f"Kedves @{chatuser.visible_name}, a vásárolandó katonák számának pozitívnak kell lennie!"
        price = int(self.soldier_price * quantity)
        if chatuser.current_score < price:
            return f"Kedves {chatuser.visible_name}, nincs elég zsetonod {quantity} katonához {self.soldier_price} áron, mert ez {price} zsetonba kerülne! "
        chatuser.current_score -= price
        defending_army, _ = Army.objects.get_or_create(
            chatbot=self.chatbot,
            commander=chatuser,
            attacking_user=None,
            is_deleted=False,
            defaults={"size": 0},
        )
        defending_army.size += quantity
        defending_army.save()
        chatuser.save()
        return f"Kedves {chatuser.visible_name}, {quantity} katonát vásároltál {self.soldier_price} áron, ami {price} zsetonba került, így {chatuser.current_score} zsetonod maradt!"

    @sync_to_async
    def sell_soldiers(self, chatuser, quantity):
        if quantity < 1:
            return f"Kedves @{chatuser.visible_name}, az eladandó katonák számának pozitívnak kell lennie!"
        defending_army = self.get_home_army(chatuser)
        if not defending_army:
            return f"Kedves @{chatuser.visible_name}, nincs eladható katonád, mert nincs védekező sereged!"
        if defending_army.size < quantity:
            return f"Kedves @{chatuser.visible_name}, nincs ennyi katonád!"
        price = int(self.soldier_price * quantity)
        chatuser.current_score += price
        defending_army.size -= quantity
        defending_army.save()
        chatuser.save()
        return f"Kedves {chatuser.visible_name}, {quantity} katonát adtál el {self.soldier_price} áron, amiért {price} zsetont kaptál, így {chatuser.current_score} zsetonod van!"

    @sync_to_async
    def offer_soldiers(self, chatuser, quantity, price, offer_for=None):
        home_army = self.get_home_army(chatuser)
        if not home_army:
            return f"Kedves {chatuser.visible_name}, nincs eladható katonád, mert nincs védekező sereged!"
        if quantity > home_army.size:
            return f"Kedves {chatuser.visible_name}, nincs ennyi katonád, hogy eladhass {quantity} katonát!"
        if quantity < 1:
            return f"Kedves @{chatuser.visible_name}, az eladandó katonák számának pozitívnak kell lennie!"
        if price < 0:
            return (
                f"Kedves @{chatuser.visible_name}, az árnak nemnegatívnak kell lennie!"
            )
        home_army.size -= quantity
        home_army.save()
        _ = ArmyOffers.objects.create(
            chatbot=self.chatbot,
            offerer=chatuser,
            quantity=quantity,
            price=price,
            offer_for=offer_for,
        )
        return f"Kedves {chatuser.visible_name}, {quantity} katonát ajánlottál fel {price} áron!"

    @sync_to_async
    def get_offers(self):
        offers = ArmyOffers.objects.filter(chatbot=self.chatbot).prefetch_related(
            "offer_for"
        )
        return list(offers)

    @sync_to_async
    def accept_offer(self, chatuser, offer_id):
        qs = ArmyOffers.objects.filter(chatbot=self.chatbot, id=offer_id)
        if not qs.exists():
            return f"Kedves {chatuser.visible_name}, nincs ilyen ajánlat!"
        offer = qs.first()
        price = int(offer.price * offer.quantity)
        if chatuser.current_score < price:
            return f"Kedves {chatuser.visible_name}, nincs elég zsetonod {offer.price} áron {offer.quantity} katonára, mert az {price} zsetonba kerülne, Neked pedig csak {chatuser.current_score} zsetonod van!"
        if offer.offer_for and offer.offer_for != chatuser:
            return f"Kedves {chatuser.visible_name}, ez az ajánlat nem Neked szól!"
        chatuser.current_score -= price
        chatuser.save()
        offer.offerer.current_score += price
        offer.offerer.save()
        home_army = self.get_home_army(chatuser)
        if not home_army:
            home_army = Army.objects.create(
                chatbot=self.chatbot,
                commander=chatuser,
                attacking_user=None,
                is_deleted=False,
                size=offer.quantity,
            )
        else:
            home_army.size += offer.quantity
            home_army.save()
        offer.delete()
        return f"Kedves {chatuser.visible_name}, elfogadtad {offer.offerer.visible_name} ajánlatát, így {offer.quantity} katonát kaptál {price} zsetonért, {chatuser.current_score} zsetonod maradt!"

    @sync_to_async
    def get_price_history(self):
        return list(ArmyPriceHistory.objects.filter(chatbot=self.chatbot))

    @sync_to_async
    def get_latest_price(self, amount):
        return list(ArmyPriceHistory.objects.filter(chatbot=self.chatbot).order_by("-time")[:amount])
