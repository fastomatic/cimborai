#!/bin/bash
# This script assumes that the first parameter, $1, is a video ID
mkdir "$1"
cd "$1"
yt-dlp https://www.youtube.com/watch?v=$1
FIRST_FILE=$(ls | head -n 1)

# Check if the folder is empty
if [ -z "$FIRST_FILE" ]; then
  echo "No files found in the folder." >> ../$1.err
else
  # Output file name
  OUTPUT_FILE="../music_cache/$1.mp3"

  # Convert to MP3 using ffmpeg
  ffmpeg -i "$FIRST_FILE" "$OUTPUT_FILE"
  rm "$FIRST_FILE"
fi

cd ..
rm -rf "$1"
