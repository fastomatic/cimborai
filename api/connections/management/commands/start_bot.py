import concurrent.futures
from django.core.management.base import BaseCommand

from connections.platforms.twitch import Bot
from twitchio.ext import routines
from chatengine.engine import ChatEngine  # pylint: disable=no-name-in-module
from chatengine.models import ChatBot
from chatengine.scheduler import run_tasks  # pylint: disable=no-name-in-module


@routines.routine(seconds=2.0)
async def run_scheduled_task(twitch_bot, chatengine):
    messages = await run_tasks("twitch", chatengine)
    if twitch_bot.default_channel:
        for message in messages:
            await twitch_bot.send(twitch_bot.default_channel, message)


class Command(BaseCommand):
    help = "Starts the bot on Twitch"

    def add_arguments(self, parser):
        # Create an argument for nickname of the bot
        parser.add_argument("--nickname", type=str)

    def handle(self, *args, **options):  # pylint: disable=unused-argument
        # Get the nickname from the options
        nickname = options["nickname"]
        chatbot = ChatBot.objects.get(nickname=nickname)
        chatengine = ChatEngine(chatbot=chatbot)
        bot = Bot(chatengine=chatengine)
        with concurrent.futures.ThreadPoolExecutor() as executor:
            future_bot = executor.submit(bot.run)
            future_scheduler = executor.submit(
                run_scheduled_task.start, bot, chatengine
            )
            concurrent.futures.wait([future_bot, future_scheduler])
        print("A miracle has happened, both the bot and the scheduler are done!")
