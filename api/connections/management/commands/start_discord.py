from chatengine.engine import ChatEngine  # pylint: disable=no-name-in-module
from chatengine.models import ChatBot
from connections.platforms.discord_connection import get_discord_client, set_chatengine
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Starts the bot on Twitch"

    def __init__(self):
        super().__init__()
        self.chatengine = None

    def add_arguments(self, parser):
        # Create an argument for nickname of the bot
        parser.add_argument("--nickname", type=str)

    def handle(self, *args, **options):  # pylint: disable=unused-argument
        # Get the nickname from the options
        nickname = options["nickname"]
        chatbot = ChatBot.objects.get(nickname=nickname)
        self.chatengine = ChatEngine(chatbot=chatbot)
        set_chatengine(self.chatengine)
        discord_client = get_discord_client()
        discord_client.run(chatbot.discord_access_token)
