import asyncio
from django.core.management.base import BaseCommand

from connections.platforms.localbot import Localbot
from chatengine.engine import ChatEngine  # pylint: disable=no-name-in-module
from chatengine.models import ChatBot

class Command(BaseCommand):
    help = "Starts the bot locally for testing purposes"

    def add_arguments(self, parser):
        # Create an argument for nickname of the bot
        parser.add_argument("--nickname", type=str)

    def handle(self, *args, **options):
        # Get the nickname from the options
        nickname = options["nickname"]
        chatbot = ChatBot.objects.get(nickname=nickname)
        chatengine = ChatEngine(chatbot=chatbot)
        bot = Localbot(chatengine=chatengine)
        loop = asyncio.get_event_loop()
        loop.run_until_complete(bot.run())
