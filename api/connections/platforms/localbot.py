
class Localbot:

    def __init__(self, chatengine):
        self.chatengine = chatengine

    async def run(self):
        current_nickname = "user"
        while True:
            message = input(f"{current_nickname}: ")
            if message.startswith("/nick"):
                current_nickname = message.split(" ")[1]
                print(f"User: {current_nickname}")
                continue
            response = await self.chatengine.get_response("local", current_nickname, message, is_whisper=False)
            print(f"Bot: {response}")
