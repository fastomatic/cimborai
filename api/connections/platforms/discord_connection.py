import os

import discord
from discord.ext import tasks
from chatengine.scheduler import run_tasks  # pylint: disable=no-name-in-module


intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

chatengine = None
SOURCE_NAME = "discord"
default_channel = ""
MESSAGE_SIZE_LIMIT = 2000


@tasks.loop(seconds=2.0)
async def run_scheduled_task(discord_client):  # pylint: disable=unused-argument
    messages = await run_tasks("discord", chatengine)
    if get_default_channel():
        for message in messages:
            while message:
                await get_default_channel().send(message[:MESSAGE_SIZE_LIMIT])
                message = message[MESSAGE_SIZE_LIMIT:]


def set_chatengine(ce):
    global chatengine  # pylint: disable=global-statement
    chatengine = ce


def get_discord_client():
    return client


@client.event
async def on_ready():
    print(f"We have logged in as {client.user}")
    run_scheduled_task.start(client)


def get_default_channel():
    return default_channel

current_music_id = None

def get_youtube_music(video_id):
    if not os.path.isdir("music_cache"):
        os.makedirs("music_cache")
    if os.path.isfile(f"music_cache/{video_id}.mp3"):
        return f"music_cache/{video_id}.mp3", ""
    if os.path.isfile(f"{video_id}.err"):
        return None, "Hiba lépett fel a zenemuzsika lefelé történő töltése közben"
    # We need to check if there is a folder with the name of the video_id
    if os.path.isdir(video_id):
        return None, "A zenemuzsika éppen most töltődik lefelé, kicsit legyél türelmes"
    # We must invoke the script "get_music_from_video.sh", pass the video ID, and let it run in the background
    os.system(f"./get_music_from_video.sh {video_id} &")
    return None, "A zenemuzsika letöltését sovány malac vágtájában megkezdtem, kitartás"

async def join_voice_channel():
    voice_channel = discord.utils.get(client.guilds[0].voice_channels)
    await voice_channel.connect()

async def play_music(file_to_play):
    voice = discord.utils.get(client.voice_clients, guild=client.guilds[0])
    if voice.is_playing():
        voice.stop()
    voice.play(discord.FFmpegPCMAudio(file_to_play))

async def play_youtube_music(url, channel, author_mention):
    global current_music_id  # pylint: disable=global-statement
    video_id = url.split("watch?v=")[1].split("&")[0]
    if current_music_id == video_id:
        await channel.send(f"Már játszom ezt a zenét, {author_mention}!")
        return
    file_to_play, status_message = get_youtube_music(video_id)
    if not file_to_play or not os.path.isfile(file_to_play):
        await channel.send(f"{status_message}, {author_mention}!")
        return
    current_music_id = video_id
    await join_voice_channel()
    await play_music(file_to_play)
    await channel.send(f"Már játszom is, {author_mention}!")


@client.event
async def on_message(message):
    global default_channel  # pylint: disable=global-statement
    if not default_channel:
        default_channel = client.get_channel(int(chatengine.chatbot.discord_default_channel_id))
    if message.author == client.user:
        return

    # It's a whisper if the message.channel is DMChannel
    is_whisper = isinstance(message.channel, discord.channel.DMChannel)
    if is_whisper:
        print(f"{message.author} /whisper/: {message.content}")
    else:
        print(f"{message.author} /{message.channel.name}/: {message.content}")

    if message.content.startswith("!play"):
        can_play_music = await chatengine.can_play_music(message.author.name)
        if not can_play_music:
            await message.channel.send(f"Nincs jogosultságod zenét játszani, {message.author.mention}!")
            return
        await play_youtube_music(message.content[5:].strip(), message.channel, message.author.mention)
        return

    roles = []
    if isinstance(message.author, discord.Member):
        roles = [role.name for role in message.author.roles]

    response = await chatengine.get_response(
        SOURCE_NAME,
        message.author.name,
        message.content,
        is_whisper=is_whisper,
        is_subscriber=False,
        is_mod=False,
        is_vip=False,
        annotations=roles,
    )

    # We need to cut it up to 4000 long parts
    while response:
        await message.channel.send(response[:MESSAGE_SIZE_LIMIT])
        response = response[MESSAGE_SIZE_LIMIT:]
