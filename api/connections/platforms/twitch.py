import datetime
import asyncio
from twitchio.ext import commands


class Bot(commands.Bot):

    SOURCE_NAME = "twitch"
    MESSAGE_FREQUENCY = 2.0

    def __init__(self, chatengine):
        # Initialise our Bot with our access token, prefix and a list of channels to join on boot...
        # prefix can be a callable, which returns a list of strings or a string...
        # initial_channels can also be a callable which returns a list of strings...
        super().__init__(
            token=chatengine.chatbot.access_token,
            prefix=chatengine.chatbot.twitch_prefix,
            initial_channels=[f"#{chatengine.chatbot.twitch_channel}"],
        )
        self.chatengine = chatengine
        # We store last message in UNIX timestamp
        self.last_message_sent = datetime.datetime.now().timestamp()
        self.default_channel = None
        # self.client = twitchio.Client(token=chatengine.chatbot.access_token)
        # self.pubsub = pubsub.PubSub(client=self.client)

    async def event_ready(self):
        # Notify us when everything is ready!
        # We are logged in and ready to chat and use commands...
        print(f"Logged in as | {self.nick}")
        print(f"User id is | {self.user_id}")

    async def event_raw_notice(self, data):
        print(f"RAW notice: {data}")

    async def event_raw_usernotice(self, data):
        print(f"RAW usernotice: {data}")

    async def event_raw_data(self, data):
        # Sample RAW message:
        split_data = data.split(" ")
        if len(split_data) > 2 and split_data[2] == "PRIVMSG":
            # split_data[0] is key=value; format, we parse it into a dict
            data_dict = dict(item.split("=") for item in split_data[0].split(";"))
            if "display_name" in data_dict:
                author = data_dict["display_name"]
                if "badges" in data_dict:
                    badges = data_dict["badges"].split(",")
                    await self.chatengine.register_badges(author, badges)

    async def send(self, channel, message):
        if not message:
            return
        if not self.default_channel:
            self.default_channel = channel
        time_since_last_message = (
            datetime.datetime.now().timestamp() - self.last_message_sent
        )
        if time_since_last_message < self.MESSAGE_FREQUENCY:
            # We need to wait a bit before sending the next message
            await asyncio.sleep(self.MESSAGE_FREQUENCY - time_since_last_message)
        await channel.send(message)
        self.last_message_sent = datetime.datetime.now().timestamp()

    async def event_message(self, message):
        # Messages with echo set to True are messages sent by the bot...
        # For now we just want to ignore them...
        if message.echo:
            return
        print(f"{message.author.name}: {message.content}")
        # The raw_data is a set of key=value; separated by ;
        # We need to parse it into a dict
        data_dict = dict(item.split("=") for item in message.raw_data.split(";"))
        # Badges are in the format of badges=badge1/num,badge2/num,...
        # We don't need the num part
        badges = []
        if "badges" in data_dict:
            badges = [badge.split("/")[0] for badge in data_dict["badges"].split(",")]
        is_whisper = False
        if not message.channel:
            is_whisper = True
        response = await self.chatengine.get_response(
            self.SOURCE_NAME,
            message.author.name,
            message.content,
            is_whisper=is_whisper,
            is_subscriber=False if is_whisper else message.author.is_subscriber,
            is_mod=False if is_whisper else message.author.is_mod,
            is_vip=False if is_whisper else message.author.is_vip,
            annotations=badges,
        )
        if response:
            message_arr = []
            while len(response) > 500:
                # We can only cut the response on space
                cut_index = response[:500].rfind(" ")
                message_arr.append(response[:cut_index])
                response = response[cut_index:]
            message_arr.append(response)
            for response in message_arr:
                await self.send(
                    message.channel if message.channel else self.default_channel,
                    response,
                )
