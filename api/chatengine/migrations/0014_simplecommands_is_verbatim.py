# Generated by Django 4.1 on 2024-06-27 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0013_alter_chatuser_twitch_sub_tier'),
    ]

    operations = [
        migrations.AddField(
            model_name='simplecommands',
            name='is_verbatim',
            field=models.BooleanField(default=False),
        ),
    ]
