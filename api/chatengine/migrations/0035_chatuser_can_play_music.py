# Generated by Django 4.1 on 2025-01-08 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0034_chatbot_owner_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatuser',
            name='can_play_music',
            field=models.BooleanField(default=False),
        ),
    ]
