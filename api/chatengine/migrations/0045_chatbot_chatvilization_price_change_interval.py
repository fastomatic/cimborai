# Generated by Django 5.1.6 on 2025-03-04 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0044_chatbot_annotation_whitelist'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatbot',
            name='chatvilization_price_change_interval',
            field=models.IntegerField(default=60),
        ),
    ]
