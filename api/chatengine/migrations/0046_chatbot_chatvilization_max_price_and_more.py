# Generated by Django 5.1.6 on 2025-03-04 16:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0045_chatbot_chatvilization_price_change_interval'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatbot',
            name='chatvilization_max_price',
            field=models.FloatField(default=5.0),
        ),
        migrations.AddField(
            model_name='chatbot',
            name='chatvilization_min_price',
            field=models.FloatField(default=1.0),
        ),
        migrations.AddField(
            model_name='chatbot',
            name='chatvilization_rounding_precision',
            field=models.IntegerField(default=5),
        ),
    ]
