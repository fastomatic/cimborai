# Generated by Django 4.1 on 2024-06-13 15:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0007_chatbot_generic_prompt'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChatUser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('platform', models.CharField(max_length=200)),
                ('username', models.CharField(max_length=200)),
                ('callname', models.CharField(blank=True, max_length=200, null=True)),
                ('chatbot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chatengine.chatbot')),
            ],
            options={
                'unique_together': {('chatbot', 'platform', 'username')},
            },
        ),
    ]
