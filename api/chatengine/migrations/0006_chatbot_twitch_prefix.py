# Generated by Django 4.1 on 2024-06-13 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatengine', '0005_chatbot_rephrase_prompt'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatbot',
            name='twitch_prefix',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
