from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from chatengine.models import (
    SimpleCommands,
    ChatBot,
    ChatUser,
    ChatLog,
    KnowledgeItems,
    RandomGiveaway,
    ScheduledMessage,
    LotteryTicket,
    NoResponseExcuses,
)


class ChatBotAdmin(ImportExportModelAdmin):
    list_display = ("nickname", "twitch_channel")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner_user=request.user)

    # From the form ,we need to hide the access token field for non-superusers
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            self.exclude = ("owner_user", )
            if "owner_user" in form.base_fields:
                form.base_fields.pop("owner_user")
        user_is_botadmin = request.user.groups.filter(name="botadmin").exists()
        if not request.user.is_superuser and not user_is_botadmin:
            self.exclude = (
                "access_token",
                "refresh_token",
                "openai_api_key",
                "token_type",
            )
            # Delete the field from the form too
            if "access_token" in form.base_fields:
                form.base_fields.pop("access_token")
                form.base_fields.pop("refresh_token")
                form.base_fields.pop("openai_api_key")
                form.base_fields.pop("token_type")
        return form


class SimpleCommandsAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "command", "response")
    list_filter = ("chatbot",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class ChatUserAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "username", "platform", "current_score")
    list_filter = ("chatbot", "username", "platform")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser and "chatbot" in form.base_fields:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class ChatLogAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "sender", "created_at", "message", "response", "context")
    list_filter = ("chatbot", "platform", "sender")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class KnowledgeItemsAdmin(ImportExportModelAdmin):
    list_editable = ("is_approved",)
    list_display = ("chatbot", "question", "answer", "is_approved")
    list_filter = ("chatbot", "is_approved",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            form.base_fields["from_chatuser"].queryset = ChatUser.objects.filter(
                chatbot__owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class RandomGiveawayAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "name", "end_at")
    list_filter = ("chatbot",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class ScheduledMessageAdmin(ImportExportModelAdmin):
    list_display = ("message", "start_at", "end_at", "interval", "is_active")
    list_filter = ("chatbot",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class LotteryTicketAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "chatuser", "guesses", "bet_amount", "is_settled")
    list_filter = ("chatbot", "chatuser", "is_settled")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
            first_bot = ChatBot.objects.filter(owner_user=request.user).first()
            if first_bot:
                form.base_fields["chatbot"].initial = first_bot.id
        return form


class NoResponseExcusesAdmin(ImportExportModelAdmin):
    list_display = ("chatbot", "excuse")
    list_filter = ("chatbot",)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(chatbot__owner_user=request.user)

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields["chatbot"].queryset = ChatBot.objects.filter(
                owner_user=request.user
            )
        return form

admin.site.register(SimpleCommands, SimpleCommandsAdmin)
admin.site.register(ChatBot, ChatBotAdmin)
admin.site.register(ChatUser, ChatUserAdmin)
admin.site.register(ChatLog, ChatLogAdmin)
admin.site.register(KnowledgeItems, KnowledgeItemsAdmin)
admin.site.register(RandomGiveaway, RandomGiveawayAdmin)
admin.site.register(ScheduledMessage, ScheduledMessageAdmin)
admin.site.register(LotteryTicket, LotteryTicketAdmin)
admin.site.register(NoResponseExcuses, NoResponseExcusesAdmin)
