import os
import random
import sys
from datetime import timedelta

from asgiref.sync import sync_to_async
from chatengine.games import (Blackjack, ChatvilizationGame, GiveawayGame,
                              Limbo, LottoGame, Roulette)
from chatengine.models import ChatLog, ChatUser, KnowledgeItems, SimpleCommands, NoResponseExcuses
from django.template import Context, Template
from django.utils import timezone
from langchain.chat_models import ChatOpenAI
from langchain.docstore.document import Document
from langchain.embeddings import CacheBackedEmbeddings
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.prompts import ChatPromptTemplate
from langchain.storage import LocalFileStore
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import FAISS  # pylint: disable=no-name-in-module
from langchain_core.messages import AIMessage, HumanMessage, SystemMessage
from slugify import slugify


class ChatEngine:
    def __init__(self, chatbot):
        self.chatbot = chatbot
        self.simple_commands = {}
        self.non_command_strings = []
        self.contained_command_strings = []
        self.openai_api_key = chatbot.openai_api_key
        self.vectorstore = None
        self.users_present = {}
        self.initialize_games()
        self.load_commands()
        self.load_vectorstore()

    def load_vectorstore(self):
        dest_dir = f"data/{slugify(self.chatbot.nickname)}/vectorstore"
        index_dir = f"data/{slugify(self.chatbot.nickname)}/index"
        if os.path.isdir(dest_dir):
            os.system(f"rm -rf {dest_dir} {index_dir}")
        underlying_embeddings = OpenAIEmbeddings(openai_api_key=self.openai_api_key)
        fs = LocalFileStore(dest_dir)
        cached_embedder = CacheBackedEmbeddings.from_bytes_store(
            underlying_embeddings, fs, namespace=underlying_embeddings.model
        )
        docs = []
        for knowledge in KnowledgeItems.objects.filter(  # pylint: disable=no-member
            chatbot=self.chatbot,
            is_approved=True,
        ):
            print(f"adding question {knowledge.id} to prompt")
            raw_documents = [
                Document(
                    metadata={
                        "source": f"knowledge{knowledge.id}",
                    },
                    page_content=f"Kérdés: {knowledge.question}\nVálasz: {knowledge.answer}",
                )
            ]
            # Split the text file into chunks of 1000 characters
            text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
            documents = text_splitter.split_documents(raw_documents)
            docs.extend(documents)
        if not docs:
            print("WARNING: Skipping prompt because it has no questions")
            return
        db = FAISS.from_documents(docs, cached_embedder)
        if not db:
            print("WARNING: Skipping prompt because it has no questions")
        else:
            db.save_local(index_dir)
        self.vectorstore = db

    def initialize_games(self):
        self.games = {
            "rulett": {
                "game_class": Roulette(self.chatbot),
                "keywords": ["rulett", "roulette"],
                "dc_only": self.chatbot.roulette_dconly,
            },
            "blackjack": {
                "game_class": Blackjack(self.chatbot),
                "keywords": ["blackjack"],
                "dc_only": self.chatbot.blackjack_dconly,
            },
            "limbo": {"game_class": Limbo(self.chatbot), "keywords": ["limbo"]},
            "giveaway": {
                "game_class": GiveawayGame(self.chatbot),
                "keywords": ["giveaway", "nyeremeny"],
                "dc_only": self.chatbot.giveaway_dconly,
            },
            "lotto": {
                "game_class": LottoGame(self.chatbot),
                "keywords": ["lotto", "lottó", "lottery"],
                "dc_only": self.chatbot.lottery_dconly,
            },
        }
        if self.chatbot.chatvilization_enabled:
            self.games["chatvilization"] = {
                "game_class": ChatvilizationGame(self.chatbot),
                "keywords": ["cv"],
                "dc_only": self.chatbot.chatvilization_dconly,
            }

    def is_user_present(self, chatuser_id, platform):
        if platform not in self.users_present:
            return False
        return chatuser_id in self.users_present[platform]

    async def update_data(self):
        # There might be something else later, e.g. user-specific data
        await sync_to_async(self.load_commands, thread_sensitive=True)()

    def load_commands(self):
        self.simple_commands = {}
        self.non_command_strings = []
        self.contained_command_strings = []
        # This is called from an async function
        for command in SimpleCommands.objects.filter(
            chatbot__nickname=self.chatbot.nickname
        ):
            if command.is_containable:
                self.contained_command_strings.append(command.command)
            else:
                self.non_command_strings.append(command.command)
            self.simple_commands[command.command] = command

    def rephrase_text(self, text):
        rephrase_prompt = self.chatbot.rephrase_prompt
        # Let's create a creative LLM and make it re-word this text
        llm = ChatOpenAI(
            model_name="gpt-4o", temperature=1.0, openai_api_key=self.openai_api_key
        )
        reword_prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    rephrase_prompt,
                ),
                ("human", "Átfogalmazandó szöveg: " + text),
            ]
        )
        chain = reword_prompt | llm
        ai_response = chain.invoke({})
        return ai_response.content

    @sync_to_async
    def generate_generic_response(
        self, chatuser, message, log
    ):  # pylint: disable=unused-argument
        messages = []
        generic_prompt = Template(self.chatbot.generic_prompt).render(
            Context(
                {
                    "chatuser": chatuser.visible_name,
                    "message": message,
                    "nickname": self.chatbot.nickname,
                    "datetime": timezone.now().strftime("%Y-%m-%d %H:%M:%S"),
                }
            )
        )
        messages.append(SystemMessage(content=generic_prompt))
        # Platform (=source) is not passed to this function, so we don't filter, but we could
        chatlog_qs = ChatLog.objects.filter(
            chatbot=self.chatbot,
            sender=chatuser.username,
            response__isnull=False,
        ).order_by("-created_at")[:10]
        prev_context = []
        prev_dialog = []
        for logentry in chatlog_qs:
            prev_context.append(SystemMessage(content=f"{logentry.context}"))
            prev_dialog.append(
                HumanMessage(content=f"{chatuser.username}: {logentry.message}")
            )
            prev_dialog.append(
                AIMessage(content=f"{self.chatbot.nickname}: {logentry.response}")
            )
        messages.extend(prev_context)
        messages.extend(prev_dialog)
        llm = ChatOpenAI(
            model_name="gpt-4o", temperature=0.7, openai_api_key=self.openai_api_key
        )
        if self.vectorstore:
            relevant_documents = self.vectorstore.similarity_search_with_score(message)
            rag_documents = ""
            for relevant_document, similarity_score in relevant_documents:
                doc_content = (
                    f"(score: {similarity_score})\n{relevant_document.page_content}"
                )
                rag_documents += doc_content
                log.context += f"\n{doc_content}"
            messages.append(
                SystemMessage(
                    content=f"Háttérinformációk a következő kérdéshez:\n{rag_documents}"
                )
            )
        messages.append(HumanMessage(content=message))
        log.debug_context = "\n".join([str(m.content) for m in messages])
        ai_response = llm.invoke(messages)
        return ai_response.content

    @sync_to_async
    def get_message_count_in_time_window(self, sender, time_window):
        msg_count = ChatLog.objects.filter(
            chatbot=self.chatbot,
            sender=sender,
            created_at__gte=timezone.now() - timedelta(minutes=time_window)
        ).count()
        return msg_count

    @sync_to_async
    def get_or_create_chatuser(self, nickname, platform):
        chatuser, _ = ChatUser.objects.get_or_create(
            chatbot=self.chatbot,
            platform=platform,
            username=nickname,
            defaults={"current_score": 1000, "can_teach": True},
        )
        # Need to load related records of chatuser
        chatuser = ChatUser.objects.select_related("current_knowledge").get(
            pk=chatuser.pk
        )
        seen_usernames = [chatuser.username]
        while (
            chatuser.parent_user and chatuser.parent_user.username not in seen_usernames
        ):
            chatuser = chatuser.parent_user
            seen_usernames.append(chatuser.username)
        return chatuser

    async def register_badges(self, nickname, badges):
        # chatuser = await self.get_or_create_chatuser(nickname)
        pass
        # await sync_to_async(chatuser.save)()

    async def learn_knowledge(self, chatuser, message):
        # Cut down everything before the first space
        if not chatuser.can_teach:
            return self.rephrase_text(
                "Nem vagyok biztos benne, hogy jó ötlet Rád hallgatni. Úgy hallottam, nagy tréfamester vagy."
            )
        question = message[message.find(" ") :].strip()
        if not question:
            return "Kérlek add meg a kérdést, például így: !tanulj Hány füle van rozsdásspatulának?"
        knowledge = KnowledgeItems(
            chatbot=self.chatbot,
            question=question,
            is_approved=False,
            from_chatuser=chatuser,
        )
        await sync_to_async(knowledge.save)()
        chatuser.current_knowledge = knowledge
        await sync_to_async(chatuser.save)()
        return "Köszönöm, a kérdést rögzítettem! És mi rá a válasz?"

    @sync_to_async
    def process_birthday(self, chatuser, message):
        params = message.split(" ")
        if len(params) != 2:
            return f"Kedves {chatuser.visible_name}, kérlek add meg a dátumot, például így: !birthday 2025-01-01"
        date = params[1]
        try:
            chatuser.birthday = date
            chatuser.save()
            return f"Kedves {chatuser.visible_name}, köszönöm, a dátumot rögzítettem!"
        except Exception:
            return f"Kedves {chatuser.visible_name}, a dátum nem megfelelő formátumú. Kérlek add meg újra, például így: !birthday 2025-01-01"

    @sync_to_async
    def process_nameday(self, chatuser, message):
        params = message.split(" ")
        if len(params) != 2:
            return f"Kedves {chatuser.visible_name}, kérlek add meg a dátumot, például így: !nameday 2025-01-01"
        date = params[1]
        try:
            chatuser.nameday = date
            chatuser.save()
            return f"Kedves {chatuser.visible_name}, köszönöm, a dátumot rögzítettem!"
        except Exception:
            return f"Kedves {chatuser.visible_name}, a dátum nem megfelelő formátumú. Kérlek add meg újra, például így: !nameday 2025-01-01"

    async def can_play_music(self, nickname):
        chatuser = await self.get_or_create_chatuser(nickname, "discord")
        return chatuser.can_play_music

    @sync_to_async
    def wipe_everything(self):
        responses = []
        # First step: reset points to 1000 for all chatusers on all platforms
        for chatuser in ChatUser.objects.filter(chatbot=self.chatbot):
            chatuser.current_score = 1000
            chatuser.save()
        responses.append("Az összes pontszám visszaállítva 1000-re.")
        # Second step: delete all armies from ChatVilization
        if "chatvilization" in self.games:
            self.games["chatvilization"]["game_class"].wipe_armies()
            responses.append("Az összes hadsereg, ajánlat és előtörténet törölve.")
        return " ".join(responses)

    async def get_response(
        self, source, sender, message, is_whisper, *args, **kwargs
    ):  # pylint: disable=unused-argument
        await sync_to_async(self.chatbot.refresh_from_db)()
        if self.chatbot.annotation_whitelist and not is_whisper:
            if "annotations" not in kwargs:
                return
            whitelisted_annotations = [annot.strip() for annot in self.chatbot.annotation_whitelist.split(",")]
            annotations = kwargs["annotations"]
            if not any(annotation in whitelisted_annotations for annotation in annotations):
                return
        chatuser = await self.get_or_create_chatuser(sender, source)
        log = await sync_to_async(ChatLog.objects.create)(
            chatbot=self.chatbot,
            platform=source,
            sender=sender,
            message=message,
            chatuser=chatuser,
            context=f"User annotations: {kwargs['annotations']}\n",
        )
        await sync_to_async(log.save)()
        await self.update_data()
        if source not in self.users_present:
            self.users_present[source] = set()
        if sender not in self.users_present[source]:
            self.users_present[source].add(chatuser.id)
        response = ""
        command = ""
        non_command = ""
        base_bonus_points = 0
        if chatuser.current_knowledge:
            chatuser.current_knowledge.answer = message
            await sync_to_async(chatuser.current_knowledge.save)()
            chatuser.current_knowledge = None
            await sync_to_async(chatuser.save)()
            return self.rephrase_text(
                "Köszönöm a választ, felírtam! Jóváhagyás után fogom is tudni."
            )
        # Need the count of messages in the last 10 minutes
        msg_count = await self.get_message_count_in_time_window(sender, self.chatbot.limit_time_window)
        if msg_count > self.chatbot.limit_message_count:
            excuses = await sync_to_async(NoResponseExcuses.objects.filter(chatbot=self.chatbot))()
            if excuses.exists():
                excuse_template = random.choice(excuses)
                context = Context({"chatuser": chatuser.visible_name, "message": message})
                excuse = Template(excuse_template.excuse).render(context)
                return excuse
            else:
                excuse = f"Az utolsó {self.chatbot.limit_time_window} percben túl sok üzeneted volt, hogy válaszoljak, kedves {chatuser.visible_name}."
                return self.rephrase_text(excuse)
        # Is the beginning of message.content any of the non command strings?
        matching_non_commands = [
            s for s in self.non_command_strings if message.lower().startswith(s)
        ] + [s for s in self.contained_command_strings if s in message.lower()]
        game = None
        for game_obj in self.games.values():
            for game_keyword in game_obj["keywords"]:
                if message.lower().startswith(
                    f"{self.chatbot.twitch_prefix}{game_keyword}"
                ):
                    game = game_obj
                    break
        points_commands = ["pontszám", "pontszam", "points"]
        leaderboard_commands = ["ranglista", "leaderboard", "toplista"]
        learn_commands = ["tanulj", "learn", "tanács"]
        if any(
            [
                message.startswith(f"{self.chatbot.twitch_prefix}{c}")
                for c in points_commands
            ]
        ):
            response = f"Kedves @{chatuser.visible_name}, jelenleg {chatuser.current_score} pontod van."
        elif any(
            [
                message.startswith(f"{self.chatbot.twitch_prefix}{c}")
                for c in learn_commands
            ]
        ):
            response = await self.learn_knowledge(chatuser, message)
        elif message.startswith(f"{self.chatbot.twitch_prefix}reload"):
            if chatuser.can_close_giveaway:
                await sync_to_async(self.load_vectorstore)()
                response = "Tudásbázis újraolvasva."
            else:
                response = f"Nincs jogosultságod ehhez a parancshoz, kedves @{chatuser.visible_name}."
        elif message.startswith(f"{self.chatbot.twitch_prefix}restart"):
            if chatuser.can_close_giveaway:
                # we need to stop the running process with all its async subprocesses
                # the script that started it will automatically restart
                sys.exit(0)
            else:
                response = f"Nincs jogosultságod ehhez a parancshoz, kedves @{chatuser.visible_name}."
        elif message.startswith(f"{self.chatbot.twitch_prefix}wipe"):
            if chatuser.can_close_giveaway:
                response = await self.wipe_everything()
            else:
                response = f"Nincs jogosultságod ehhez a parancshoz, kedves @{chatuser.visible_name}."
        elif message.startswith(f"{self.chatbot.twitch_prefix}birthday"):
            response = await self.process_birthday(chatuser, message)
        elif message.startswith(f"{self.chatbot.twitch_prefix}nameday"):
            response = await self.process_nameday(chatuser, message)
        elif any(
            [
                message.startswith(f"{self.chatbot.twitch_prefix}{c}")
                for c in leaderboard_commands
            ]
        ):
            leaderboard = ""
            leaders_cnt = 0
            pos = 1
            qs = ChatUser.objects.filter(chatbot=self.chatbot).order_by(
                "-current_score"
            )
            for user in await sync_to_async(list)(qs):
                if leaders_cnt < 5:
                    leaderboard += f"{user.visible_name}: {user.current_score} pont, "
                    leaders_cnt += 1
                if user.username == chatuser.username:
                    response = f"Kedves @{chatuser.visible_name}, a helyezésed: {pos}. "
                pos += 1
            response += f"Top 5: {leaderboard}"
        elif game:
            if not game["dc_only"] or source in ["discord", "local"]:
                response = await game["game_class"].player_move(
                    chatuser, message, log, is_whisper
                )
            else:
                response = f"Kedves @{chatuser.visible_name}, a játékokat csak a Discordon tudod játszani, a Kaszinó szobában."
        elif source == "discord" and is_whisper and message.startswith("!connect"):
            chatuser.connect_code = str(random.randint(1000, 9999))
            response = f"Kedves {chatuser.visible_name}, a kapcsolódási kódod: {chatuser.connect_code}. Kérlek add meg a Twitch chaten SUTTOGÓ ÜZENETBEN a !connect {chatuser.connect_code} parancsot a kapcsolódáshoz."
        elif source == "twitch" and is_whisper and message.startswith("!connect"):
            params = message.split(" ")[1:]
            if len(params) != 1:
                response = "Kérlek add meg a kapcsolódási kódot."
            else:
                connect_code = params[0]
                qs = ChatUser.objects.filter(
                    chatbot=self.chatbot, connect_code=connect_code
                )
                if await sync_to_async(qs.exists)():
                    parent_user = await sync_to_async(qs.first)()
                    chatuser.parent_user = parent_user
                    parent_user.current_score += chatuser.current_score - 1000
                    await sync_to_async(parent_user.save)()
                    response = f"Kedves {chatuser.visible_name}, sikeresen kapcsolódtál a Discordon. Most már tudsz játszani a játékokkal."
                else:
                    response = "Hibás kapcsolódási kód."
            print(f"Response to connect request: {response}")
        elif matching_non_commands:
            non_command = matching_non_commands[0]
            template = Template(self.simple_commands[non_command].response)
            context = Context({"chatuser": chatuser.visible_name, "message": message})
            response = template.render(context)
            if not self.simple_commands[non_command].is_verbatim:
                response = self.rephrase_text(response)
        elif source == "twitch" and message.startswith(self.chatbot.twitch_prefix):
            command = message[1:]
            method = getattr(self, "on_" + command, None)
            if method:
                response = method(chatuser, message)  # pylint: disable=not-callable
                response = self.rephrase_text(response)
        elif (
            source == "twitch"
            and f"@{self.chatbot.nickname.lower()}" in message.lower()
        ):
            response = await self.generate_generic_response(chatuser, message, log)
            base_bonus_points += 1
        elif source == "discord" and self.chatbot.discord_bot_id in message:
            response = await self.generate_generic_response(chatuser, message, log)
            base_bonus_points += 1
        elif source == "discord" and is_whisper:
            response = await self.generate_generic_response(chatuser, message, log)
        elif source == "local":
            base_bonus_points += 1
            response = await self.generate_generic_response(chatuser, message, log)
        else:
            base_bonus_points += 1

        # Since the LLM gets the chat history with the speaker name first, it returns it the same way
        # So we need to remove the bot's name, if the response begins with it, and a semicolon
        while response.startswith(f"{self.chatbot.nickname}: "):
            response = response[len(f"{self.chatbot.nickname}: ") :]
        if response.startswith(": "):
            response = response[2:]

        if "is_subscriber" in kwargs and kwargs["is_subscriber"]:
            base_bonus_points = base_bonus_points * 2
        elif "is_vip" in kwargs and kwargs["is_vip"]:
            base_bonus_points = base_bonus_points * 2
        elif "is_mod" in kwargs and kwargs["is_mod"]:
            base_bonus_points = base_bonus_points * 2
        await sync_to_async(chatuser.refresh_from_db)()
        chatuser.current_score += base_bonus_points
        await sync_to_async(chatuser.save)()
        log.context += f"\nFinal user score is {chatuser.current_score}, after adding {base_bonus_points} points.\n"
        log.response = response
        await sync_to_async(log.save)()
        return response
