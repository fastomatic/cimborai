import random
import datetime
from asgiref.sync import sync_to_async

from chatengine.games.chatgame import ChatGame
from chatengine.models import LotteryTicket
from chatengine.scheduler import get_scheduler


class LottoGame(ChatGame):
    def __init__(self, chatbot):
        super().__init__(chatbot)
        self.last_draw = None
        self.last_log = datetime.datetime.now()
        get_scheduler().add_task(self.check_game_state)

    async def player_move(self, chatuser, move, log, is_whisper=False):
        log_context = ""
        instructions = f"Kedves @{chatuser.visible_name}, a !lotto parancshoz add meg az öt számot 1 és 50 között, és a tétet. Például: !lotto 5 12 23 34 45 10."
        if move == f"{self.chatbot.twitch_prefix}lotto":
            return instructions
        params = move.split(" ")[1:]
        if len(params) != 6:
            return instructions
        *numbers, amount = params
        if not all(num.isdigit() for num in numbers) or not amount.isdigit():
            return f"Kedves @{chatuser.visible_name}, minden szám és a tét csak szám lehet."
        if any(int(num) < 1 or int(num) > 50 for num in numbers):
            return f"Kedves @{chatuser.visible_name}, minden számnak 1 és 50 között kell lennie."
        if int(amount) < 1:
            return f"Kedves @{chatuser.visible_name}, a tét csak pozitív szám lehet."
        if int(amount) > chatuser.current_score:
            return f"Kedves @{chatuser.visible_name}, nincs ennyi pontod. Jelenlegi pontszámod: {chatuser.current_score}."

        numbers = [int(num) for num in numbers]
        # The numbers must be different from each other, i.e. you can't bet 5 5 5 5 5
        if len(numbers) != len(set(numbers)):
            return f"Kedves @{chatuser.visible_name}, 5 egymástól különböző számot kell megadnod."
        amount = int(amount)

        log_context += f"Username: {chatuser.username}, chosen numbers: {numbers}, bet amount: {amount}, score before: {chatuser.current_score}\n"
        ticket = LotteryTicket(
            chatbot=self.chatbot,
            chatuser=chatuser,
            guesses=numbers,
            bet_amount=amount,
            is_settled=False,
        )
        await sync_to_async(ticket.save)()
        chatuser.current_score -= ticket.bet_amount
        await sync_to_async(chatuser.save)()
        return f"Kedves @{chatuser.visible_name}, a szelvényedet rögzítettük. A tét: {amount} pont."

    async def check_game_state(self, log, platform):
        if platform != "discord":
            log.context += f"Lottery game is only available on Discord, not on {platform}\n"
            return ""
        response = ""
        # We do lottery drawing at 20:00 every day
        # TODO change lottery to happen every 23 hours
        saved_now = datetime.datetime.now()
        time_since_last_draw = (
            (saved_now - self.last_draw).seconds if self.last_draw else 100
        )
        if (
            time_since_last_draw > 10
            and saved_now.hour == 18 # Server is GMT, so 18 GMT is 20 CET
            and saved_now.minute == 0
            and saved_now.second < 9
        ):
            self.last_draw = saved_now
            response = await self.settle_game(log)
            print(f"Lottery settled at {saved_now}")
        else:
            log.context += f"Time since last draw: {time_since_last_draw}, last draw: {self.last_draw}, now: {saved_now}\n"
            if saved_now - self.last_log > datetime.timedelta(minutes=1):
                print(f"Time since last draw: {time_since_last_draw}, last draw: {self.last_draw}, now: {saved_now}")
                self.last_log = saved_now
        return response

    @sync_to_async
    def get_unsettled_tickets(self):
        return list(
            LotteryTicket.objects.filter(is_settled=False).select_related("chatuser")
        )

    async def settle_game(self, log):
        drawn_numbers = random.sample(range(1, 51), 5)
        log.context += f"Drawn numbers: {drawn_numbers}"
        response = f"A lottósorsolás eredménye: {drawn_numbers}. "
        tickets = await self.get_unsettled_tickets()
        for ticket in tickets:
            response += await self.check_ticket(ticket, drawn_numbers, log)
            response += " "
        return response

    async def check_ticket(self, ticket, drawn_numbers, log):
        log_context = ""
        numbers = ticket.guesses
        matches = set(numbers) & set(drawn_numbers)
        num_matches = len(matches)

        # Ezen még alakítani kellene, mert igazságtalan szerintem.
        win_multiplier = {0: 0, 1: 2, 2: 14, 3: 214, 4: 9416, 5: 2118760}
        win_amount = ticket.bet_amount * win_multiplier[num_matches]

        if win_amount > 0:
            ticket.chatuser.current_score += win_amount
            response = f"Gratulálok @{ticket.chatuser.visible_name}, {num_matches} találatod volt ({matches})! Nyertél {win_amount} pontot! Az új pontszámod: {ticket.chatuser.current_score}."
        else:
            response = f"Sajnos nem nyertél, @{ticket.chatuser.visible_name}. A tipped: {ticket.guesses}. Miután veszítettél {ticket.bet_amount} pontot, az új pontszámod: {ticket.chatuser.current_score}."

        log_context += f"Drawn numbers: {drawn_numbers}, matches: {matches}, score after: {ticket.chatuser.current_score}"
        log.context = log_context
        await sync_to_async(ticket.chatuser.save)()
        ticket.is_settled = True
        await sync_to_async(ticket.save)()
        return response
