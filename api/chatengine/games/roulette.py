import random
from asgiref.sync import sync_to_async

from chatengine.games.chatgame import ChatGame


class Roulette(ChatGame):

    RED_NUMBERS = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36]
    BLACK_NUMBERS = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35]

    def number_name(self, number):
        if number == 0:
            return "0 (sem szín, sem páros/páratlan tulajdonsága nincs)"
        even = "páros" if number % 2 == 0 else "páratlan"
        color = "piros" if number in self.RED_NUMBERS else "fekete"
        return f"{number}, {even}, {color}"

    def win_msg(self, chatuser, rolled_result, prize):
        return f"Gratulálok @{chatuser.visible_name}, nyertél {prize} pontot! Az eredmény: `{self.number_name(rolled_result)}`. Az új pontszámod: {chatuser.current_score}."

    def lose_msg(self, chatuser, rolled_result, lost_bet):
        return f"Sajnos nem nyertél, @{chatuser.visible_name}, az eredmény: `{self.number_name(rolled_result)}`. Miután veszítettél {lost_bet} pontot, az új pontszámod: {chatuser.current_score}."

    async def player_move(self, chatuser, move, log, is_whisper=False):
        log_context = ""
        response = ""
        instructions = f"Kedves @{chatuser.visible_name}, a !rulett parancshoz add meg, hogy mire teszel és mennyit. Tehetsz az alábbiakra: piros, fekete, konkrét szám, páros, páratlan, alacsony (1-18), magas (19-36), 1.kupac (1-12), 2.kupac (13-24), 3.kupac (25-36), 1.oszlop (1,4,7 ...), 2.oszlop (2, 5, 8 ...), 3.oszlop (3, 6, 9 ...)"
        if move == f"{self.chatbot.twitch_prefix}rulett":
            return instructions
        params = move.split(" ")[1:]
        if len(params) != 2:
            return instructions
        bet, amount = params
        if not amount.isdigit():
            return f"Kedves @{chatuser.visible_name}, a tét csak szám lehet."
        if int(amount) < 1:
            return f"Kedves @{chatuser.visible_name}, a tét csak pozitív szám lehet."
        if bet not in ["piros", "fekete", "páros", "páratlan", "alacsony", "magas", "1.kupac", "2.kupac", "3.kupac", "1.oszlop", "2.oszlop", "3.oszlop"] and not bet.isdigit():
            return f"Kedves @{chatuser.visible_name}, a tét csak piros, fekete, páros, páratlan, alacsony, magas, (1,2,3).kupac, (1,2,3).oszlop vagy egy konkrét szám lehet. Kupac és oszlop szóköz nélkül."
        if int(amount) > chatuser.current_score:
            return f"Kedves @{chatuser.visible_name}, nincs ennyi pontod. Jelenlegi pontszámod: {chatuser.current_score}."
        amount = int(amount)
        if bet.isdigit() and (int(bet) < 0 or int(bet) > 36):
            return f"Kedves @{chatuser.visible_name}, a rulett asztalon csak 0-tól 36-ig terjednek a számok."
        rolled_result = random.randint(0, 36)
        log_context += f"Username: {chatuser.username}, bet amount: {amount}, rolled result: {rolled_result}, score before: {chatuser.current_score}\n"
        if bet.isdigit():
            if int(bet) == rolled_result:
                chatuser.current_score += amount*36
                response = self.win_msg(chatuser, rolled_result, amount*36)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)
        if bet in ["piros", "fekete"]:
            if (bet == "piros" and rolled_result in self.RED_NUMBERS) or (
                bet == "fekete" and rolled_result in self.BLACK_NUMBERS
            ):
                chatuser.current_score += amount
                response = self.win_msg(chatuser, rolled_result, amount)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)
        elif bet in ["páros", "páratlan"]:
            if (rolled_result != 0 and
                bet == "páros" and rolled_result % 2 == 0 or
                bet == "páratlan" and rolled_result % 2 == 1
            ):
                chatuser.current_score += amount
                response = self.win_msg(chatuser, rolled_result, amount)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)
        elif bet in ["alacsony", "magas"]:
            if (rolled_result != 0 and (
                bet == "alacsony" and rolled_result <= 18 or
                bet == "magas" and rolled_result >= 19
            )):
                chatuser.current_score += amount
                response = self.win_msg(chatuser, rolled_result, amount)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)
        elif bet in ["1.kupac", "2.kupac", "3.kupac"]:
            if (rolled_result != 0 and (
                bet == "1.kupac" and rolled_result <= 12 or
                bet == "2.kupac" and rolled_result >= 13 and rolled_result <= 24 or
                bet == "3.kupac" and rolled_result >= 25
            )):
                chatuser.current_score += amount * 6
                response = self.win_msg(chatuser, rolled_result, amount * 6)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)
        elif bet in ["1.oszlop", "2.oszlop", "3.oszlop"]:
            if (rolled_result != 0 and (
                bet == "1.oszlop" and rolled_result % 3 == 1 or
                bet == "2.oszlop" and rolled_result % 3 == 2 or
                bet == "3.oszlop" and rolled_result % 3 == 0
            )):
                chatuser.current_score += amount * 6
                response = self.win_msg(chatuser, rolled_result, amount * 6)
            else:
                chatuser.current_score -= amount
                response = self.lose_msg(chatuser, rolled_result, amount)

        log_context += f"Score after: {chatuser.current_score}"
        log.context = log_context
        await sync_to_async(chatuser.save)()
        return response
