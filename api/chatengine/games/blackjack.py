import datetime
import random
from asgiref.sync import sync_to_async
from chatengine.games.chatgame import ChatGame
from chatengine.scheduler import get_scheduler


class Blackjack(ChatGame):

    STATE_NOT_STARTED = "not_started"
    STATE_WAITING_FOR_BETS = "waiting_for_bets"
    STATE_WAITING_FOR_PLAYERS = "waiting_for_players"
    STATE_DEAL_HANDS = "deal_hands"
    STATE_WAITING_FOR_MOVES = "waiting_for_moves"

    def __init__(self, chatbot):
        super().__init__(chatbot)
        self.players = {}
        self.game_state = ""
        self.game_started = 0
        self.own_hand = {}
        self.deck = []
        self.initialize_game()
        get_scheduler().add_task(self.check_game_state)

    def initialize_game(self):
        self.players = {}
        self.game_started = 0
        self.game_state = self.STATE_NOT_STARTED
        self.own_hand = {}
        self.deck = list(range(0, 52))
        random.shuffle(self.deck)

    def pull_from_deck(self):
        card = self.deck.pop()
        if len(self.deck) < 10:
            self.deck = list(range(0, 52))
            random.shuffle(self.deck)
        return card

    def card_to_str(self, card):
        suits = ["♠", "♣", "♦", "♥"]
        values = [
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "J",
            "Q",
            "K",
            "A",
        ]
        suit = card // 13
        value = card % 13
        return f"{suits[suit]}{values[value]}"

    def add_player_to_game(self, chatuser, bet):
        self.players[chatuser.username] = chatuser
        self.players[chatuser.username].bet = bet
        self.players[chatuser.username].did_stand = False
        self.players[chatuser.username].is_busted = False
        self.players[chatuser.username].hand_value = 0
        chatuser.current_score -= bet

    def get_hand_value(self, hand):
        # If the card is < 10, the value is the card itself + 2
        # If the card is 10, J, Q, K, the value is 10
        # If the card is A, the value is 11
        hand_value = 0
        for card in hand:
            card_value = card % 13 + 2
            if card_value < 10:
                hand_value += card_value
            elif card_value < 14:
                hand_value += 10
            else:
                hand_value += 11
        # TODO: if there are multiple A's in the hand, we can subtract 10 multiple times
        if hand_value > 21 and 12 in [card % 13 for card in hand]:
            hand_value -= 10
        return hand_value

    async def settlement(self):
        response = ""
        log_context = ""
        cnt_not_standing_players = len(
            [player for player in self.players.values() if not player.did_stand]
        )
        if cnt_not_standing_players == 0:
            response += "Mindenki döntött, jöhet az osztó! "
            log_context += "All players have made their moves. Dealer's turn.\n"
            dealer_hand_value = self.get_hand_value(self.own_hand)
            while dealer_hand_value < 17:
                self.own_hand.append(self.pull_from_deck())
                dealer_hand_value = self.get_hand_value(self.own_hand)
            log_context += f"Dealer's hand: {' '.join([self.card_to_str(card) for card in self.own_hand])}. Score: {dealer_hand_value}.\n"
            response += f" Az osztó lapjai: {' '.join([self.card_to_str(card) for card in self.own_hand])}. "
            for player_tuple in self.players.items():
                player = player_tuple[1]
                player_won = False
                if player.hand_value == 21 and dealer_hand_value != 21:
                    log_context += f"{player.visible_name} has blackjack, dealer does not, so player won.\n"
                    player_won = True
                elif player.hand_value > dealer_hand_value and not player.is_busted:
                    log_context += f"{player.visible_name} has higher score than dealer, and is not busted, so player won.\n"
                    player_won = True
                elif dealer_hand_value > 21 and not player.is_busted:
                    log_context += f"Dealer is busted, so {player.visible_name} won.\n"
                    player_won = True
                if player_won:
                    player.current_score += player.bet * 2
                    await sync_to_async(player.save)()
                    log_context += f"{player.visible_name} won {player.bet*2} points.\n"
                    response += f"@{player.visible_name} nyert {player.bet*2} zsetont, ezzel már {player.current_score} zsetonja lett! "
                elif player.hand_value == dealer_hand_value and not player.is_busted:
                    player.current_score += player.bet
                    await sync_to_async(player.save)()
                    log_context += f"{player.visible_name} has the same score as dealer, so player got back the bet, which was {player.bet} points.\n"
                    response += f"@{player.visible_name} visszakapta a tétjét, ezzel már {player.current_score} zsetonja lett!! "
                else:
                    log_context += f"{player.visible_name} lost.\n"
                    response += f"@{player.visible_name} vesztett, ezzel {player.current_score} zsetonja lett!. "
            self.initialize_game()
        else:
            if cnt_not_standing_players == 1:
                not_standing_player = [
                    player for player in self.players.values() if not player.did_stand
                ][0]
                response += f"Még {not_standing_player.visible_name} döntésére várunk. "
                log_context += f"Waiting for {not_standing_player.visible_name}.\n"
            else:
                response += f"Még {cnt_not_standing_players} játékosnak kell döntenie. "
                log_context += "Waiting for "
                for player_tuple in self.players.items():
                    player = player_tuple[1]
                    if not player.did_stand:
                        log_context += f"{player.visible_name}, "
                log_context = log_context[:-2] + ".\n"
        return response, log_context

    def all_players_stand(self):
        for player_tuple in self.players.items():
            player = player_tuple[1]
            if not player.did_stand:
                player.did_stand = True

    def deal_hands(self):
        log_context = ""
        print("STATE_DEAL_HANDS")
        self.own_hand = [self.pull_from_deck(), self.pull_from_deck()]
        log_context += f"Dealer's hand: {self.card_to_str(self.own_hand[0])} {self.card_to_str(self.own_hand[1])}.\n"
        response = (
            f"Az osztó lapjai: {self.card_to_str(self.own_hand[0])} (takarva). "
        )
        for player_tuple in self.players.items():
            player = player_tuple[1]
            player.hand = [self.pull_from_deck(), self.pull_from_deck()]
            log_context += f"{player.visible_name} cards: {self.card_to_str(player.hand[0])} {self.card_to_str(player.hand[1])}.\n"
            response += f"@{player.visible_name} lapjai: {self.card_to_str(player.hand[0])} {self.card_to_str(player.hand[1])}, "
        response += "Játékosok, döntsetek! hit vagy stand. "
        self.game_state = self.STATE_WAITING_FOR_MOVES
        self.game_started = datetime.datetime.now()
        return response, log_context

    async def check_game_state(self, log, platform, do_settlement=True):
        response = ""
        if (
            self.game_state == self.STATE_WAITING_FOR_BETS
            and (datetime.datetime.now() - self.game_started).seconds > 60
        ):
            self.game_state = self.STATE_DEAL_HANDS
            response = "FIGYELEM, a blackjack kör elindult, több beszállás nincs!"
            new_response, new_log_context = self.deal_hands()
            response += new_response
            log.context += new_log_context
        elif (
            self.game_state == self.STATE_WAITING_FOR_MOVES
            and (datetime.datetime.now() - self.game_started).seconds > 60
        ):
            # all players who have not moved yet now stand
            self.all_players_stand()
            if do_settlement:
                settlement_result, settlement_log = await self.settlement()
                log.context += settlement_log
                response += settlement_result
        return response

    async def player_move(self, chatuser, move, log, is_whisper=False):
        response = ""
        log_context = ""
        settlement_log = ""
        params = move.split(" ")[1:]
        response += await self.check_game_state(log, "", do_settlement=False)
        if self.game_state == self.STATE_NOT_STARTED:
            print("STATE_NOT_STARTED")
            if len(params) != 1:
                print(f"params len not 1: {params}")
                return " a !blackjack parancshoz add meg, hogy mennyit szeretnél tenni, például: !blackjack 10."
            if not params[0].isdigit():
                return f"Kedves @{chatuser.visible_name}, a tét csak pozitív egész szám lehet."
            player_bet = int(params[0])
            if player_bet < 1:
                return f"Kedves @{chatuser.visible_name}, a tét csak pozitív egész szám lehet."
            if player_bet > chatuser.current_score:
                return f"Kedves @{chatuser.visible_name}, nincs ennyi pontod. Jelenlegi pontszámod: {chatuser.current_score}."
            # A new game has started with a player joining
            log_context += f"New game started with {chatuser.visible_name} betting {player_bet}. Original score: {chatuser.current_score}. "
            self.add_player_to_game(chatuser, player_bet)
            print("state -> waiting for bets")
            self.game_state = self.STATE_WAITING_FOR_BETS
            self.game_started = datetime.datetime.now()
            response = (
                "FIGYELEM, indul a blackjack! 1 percetek van beszállni! "
                + f"@{chatuser.visible_name} már fogadott is, tétje: {player_bet}."
            )
        elif self.game_state == self.STATE_WAITING_FOR_BETS:
            print("STATE_WAITING_FOR_BETS")
            if len(params) != 1:
                return f"Kedves @{chatuser.visible_name}, a !blackjack parancshoz add meg, hogy mennyit szeretnél tenni, például: !blackjack 10."
            player_bet = int(params[0])
            if player_bet < 1:
                return f"Kedves @{chatuser.visible_name}, a tét csak pozitív egész szám lehet."
            if player_bet > chatuser.current_score:
                return f"Kedves @{chatuser.visible_name}, nincs ennyi pontod. Jelenlegi pontszámod: {chatuser.current_score}."
            if chatuser.username in self.players:
                chatuser.current_score += self.players[chatuser.username].bet
                self.players[chatuser.username].bet = player_bet
                chatuser.current_score -= player_bet
                log_context += f"{chatuser.visible_name} already in the game, changing bet to {player_bet}. Current score is {chatuser.current_score}."
                response = f"@{chatuser.visible_name}, téted módosítva: {player_bet}."
            else:
                self.add_player_to_game(chatuser, player_bet)
                log_context += f"{chatuser.visible_name} joined the game, betting {player_bet}. Current score is {chatuser.current_score}."
                response = f"@{chatuser.visible_name} beszálltál, téted: {player_bet}."
        elif self.game_state == self.STATE_DEAL_HANDS:
            new_response, new_log_context = self.deal_hands()
            response += new_response
            log_context += new_log_context
        elif self.game_state == self.STATE_WAITING_FOR_MOVES:
            if chatuser.username not in self.players:
                return f"Kedves @{chatuser.visible_name}, ebből a játékból most kimaradtál, de várunk a következőben!"
            if len(params) != 1:
                return f"Kedves @{chatuser.visible_name}, csak hit vagy stand lehet. "
            player_move = params[0]
            player = self.players[chatuser.username]
            if player_move not in ["hit", "stand"]:
                return f"Kedves @{chatuser.visible_name}, csak hit vagy stand lehet. "
            log_context += f"{player.visible_name} move: {player_move}. "
            if player_move == "hit":
                player.hand.append(self.pull_from_deck())
                player.hand_value = self.get_hand_value(player.hand)
                if player.hand_value > 21:
                    response = f"@{player.visible_name}, vesztettél! Lapjaid: {' '.join([self.card_to_str(card) for card in player.hand])}. "
                    player.did_stand = True
                    player.is_busted = True
                    log_context += f"{player.visible_name} busted with score {player.hand_value}.\n"
                elif player.hand_value == 21:
                    response = f"@{player.visible_name}, 21 pontod van! Lapjaid: {' '.join([self.card_to_str(card) for card in player.hand])}. "
                    log_context += f"{player.visible_name} has 21.\n"
                    player.did_stand = True
                else:
                    log_context += f"{player.visible_name} hand: {' '.join([self.card_to_str(card) for card in player.hand])}. Score: {player.hand_value}.\n"
                    response = f"@{player.visible_name}, lapjaid: {' '.join([self.card_to_str(card) for card in player.hand])}. Hit vagy stand? "
            elif player_move == "stand":
                player.hand_value = self.get_hand_value(player.hand)
                response = f"@{player.visible_name}, rendben, lapjaid: {' '.join([self.card_to_str(card) for card in player.hand])}."
                log_context += f"{player.visible_name} stands with score {player.hand_value}.\n"
                player.did_stand = True
            settlement_result, settlement_log = await self.settlement()
            response += settlement_result

        log.context += log_context + settlement_log
        await sync_to_async(chatuser.save)()
        print(f"Cimbor_AI: {response}")
        return response
