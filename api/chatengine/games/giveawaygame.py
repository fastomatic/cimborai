import datetime
import random

from asgiref.sync import sync_to_async
from chatengine.games.chatgame import ChatGame
from chatengine.models import RandomGiveaway


class GiveawayGame(ChatGame):
    @sync_to_async
    def close_games(self, chatuser):
        if not chatuser.can_close_giveaway:
            return f"Kedves @{chatuser.visible_name}, nincs jogosultságod a játék lezárására."
        giveaway = self.get_first_unwon_game()
        if not giveaway:
            return f"Kedves @{chatuser.visible_name}, jelenleg nincs lezárt, nyeretlen játék."
        winner = random.choice(giveaway.participants.all())
        giveaway.winner = winner
        giveaway.save()
        response = f"Kedves @{chatuser.visible_name}, a játék lezárásra került. A nyertes: @{winner.visible_name}. Gratulálunk!"
        return response

    @sync_to_async
    def show_games(self, chatuser):
        qs = RandomGiveaway.objects.filter(
            chatbot=self.chatbot,
            start_at__lte=datetime.datetime.now(),
            end_at__gte=datetime.datetime.now(),
        )
        # add the number of participants to the queryset
        qs = qs.prefetch_related("participants")
        response = ""
        if not qs.exists():
            response = f"Kedves @{chatuser.visible_name}, jelenleg nincs aktív játék."
        else:
            giveaway = qs.first()
            number_of_participants = giveaway.participants.count()
            response = f"Kedves @{chatuser.visible_name}, a következő játék aktív: {giveaway.name} (jelentkezők száma: {number_of_participants}). {giveaway.description}"
        return response

    def get_first_unwon_game(self):
        qs = RandomGiveaway.objects.filter(
            chatbot=self.chatbot,
            start_at__lte=datetime.datetime.now(),
            end_at__lt=datetime.datetime.now(),
            winner=None,
        )
        if not qs.exists():
            return None
        else:
            giveaway = qs.first()
            return giveaway

    def get_first_active_game(self):
        qs = RandomGiveaway.objects.filter(
            chatbot=self.chatbot,
            start_at__lte=datetime.datetime.now(),
            end_at__gte=datetime.datetime.now(),
        )
        if not qs.exists():
            return None
        else:
            giveaway = qs.first()
            return giveaway

    @sync_to_async
    def join_game(self, chatuser):
        response = ""
        giveaway = self.get_first_active_game()
        if not giveaway:
            response = f"Kedves @{chatuser.visible_name}, jelenleg nincs aktív játék."
        else:
            # Is the user already in?
            if giveaway.participants.filter(id=chatuser.id).exists():
                response = f"Kedves @{chatuser.visible_name}, már részt veszel a(z) {giveaway.name} játékban."
            else:
                giveaway.participants.add(chatuser)
                response = f"Kedves @{chatuser.visible_name}, sikeresen jelentkeztél a(z) {giveaway.name} játékba."
        return response

    async def player_move(self, chatuser, move, log, is_whisper=False):
        params = move.split()
        if len(params) < 2:
            return f"Kedves @{chatuser.visible_name}, a helyes formátum: !giveaway show vagy join."
        if params[1] == "join":
            response = await self.join_game(chatuser)
            return response
        elif params[1] == "show":
            return await self.show_games(chatuser)
        elif params[1] == "close":
            return await self.close_games(chatuser)
