import math
from chatengine.games.chatgame import ChatGame
from chatvilization.chatvilization import ChatVilization

class ChatvilizationGame(ChatGame):

    def __init__(self, chatbot):
        super().__init__(chatbot)
        self.chatvilization = ChatVilization(chatbot, "discord")

    def wipe_armies(self):
        self.chatvilization.wipe_armies()

    async def player_move(self, chatuser, move, log, is_whisper=False):
        # Move begins with !cv
        params = move.split(" ")
        if len(params) < 2:
            return f"Kedves @{chatuser.visible_name}, kérlek adj meg egy parancsot és egy számot, pl. !cv buy 5"
        if params[1] in ["vásárol", "buy"]:
            if len(params) < 3:
                return f"Kedves @{chatuser.visible_name}, kérlek add meg, hogy mennyi katonát szeretnél vásárolni, pl. !cv buy 5"
            if not params[2].isdigit():
                return f"Kedves @{chatuser.visible_name}, a vásárolandó katonák számának egész számnak kell lennie!"
            quantity = int(params[2])
            return await self.chatvilization.buy_soldiers(chatuser, quantity)
        elif params[1] in ["elad", "sell"]:
            if len(params) < 3:
                return f"Kedves @{chatuser.visible_name}, kérlek add meg, hogy mennyi katonát szeretnél eladni, pl. !cv sell 5"
            if not params[2].isdigit():
                return f"Kedves @{chatuser.visible_name}, az eladandó katonák számának egész számnak kell lennie!"
            quantity = int(params[2])
            return await self.chatvilization.sell_soldiers(chatuser, quantity)
        elif params[1] in ["felajánl", "offer"]:
            if len(params) < 4:
                return f"Kedves @{chatuser.visible_name}, kérlek add meg, hogy mennyi katonát szeretnél eladni mennyiért (és kinek), pl. !cv offer 5 3.0 [@user]"
            if not params[2].isdigit():
                return f"Kedves @{chatuser.visible_name}, az eladandó katonák számának egész számnak kell lennie!"
            quantity = int(params[2])
            if not params[3].replace(".", "", 1).isdigit():
                return f"Kedves @{chatuser.visible_name}, az árnak számnak kell lennie!"
            price = float(params[3])
            offer_for = None
            if len(params) > 4:
                offer_for = await self.chatvilization.get_attackable_chatuser(params[4], chatuser)
                if isinstance(offer_for, str):
                    return f"Kedves @{chatuser.visible_name}, nem ajánlatsz {params[4]} részére sereget, mert " + offer_for
            return await self.chatvilization.offer_soldiers(chatuser, quantity, price, offer_for)
        elif params[1] in ["elfogad", "accept"]:
            if len(params) < 3:
                return f"Kedves @{chatuser.visible_name}, kérlek add meg, hogy melyik ajánlatot szeretnéd elfogadni, pl. !cv accept 1"
            if not params[2].isdigit():
                return f"Kedves @{chatuser.visible_name}, az ajánlat sorszámának egész számnak kell lennie!"
            offer_id = int(params[2])
            return await self.chatvilization.accept_offer(chatuser, offer_id)
        elif params[1] in ["ajánlatok", "offers"]:
            offers = await self.chatvilization.get_offers()
            if not offers:
                return f"Kedves @{chatuser.visible_name}, jelenleg nincsenek ajánlatok."
            response = f"Kedves @{chatuser.visible_name}, jelenleg {len(offers)} ajánlat van:"
            for offer in offers:
                response += f"{offer.id}.) {offer.quantity} katonát {offer.price} áron"
                if offer.offer_for:
                    response += f" {offer.offer_for.visible_name} részére. "
                else:
                    response += ". "
            return response
        elif params[1] in ["állapot", "status"]:
            response = f"Kedves {chatuser.visible_name}, a játék állapota: {self.chatvilization.soldier_price} ára van a katonának"
            armies = await self.chatvilization.get_armies_status(chatuser)
            for army in armies:
                if not army.attacking_user:
                    response += f", védekező sereged mérete {army.size}"
                else:
                    response += f", egy támadó sereg {army.size} méretben támadja {army.attacking_user.visible_name}"
            response += "."
            return response
        elif params[1] in ["attack", "támadás"]:
            if len(params) < 4:
                return f"Kedves @{chatuser.visible_name}, kérlek add meg, hogy melyik felhasználót szeretnéd támadni, és mekkora sereggel, pl. !cv attack @user 100"
            if not params[3].isdigit():
                return f"Kedves @{chatuser.visible_name}, a támadó sereg méretének egész számnak kell lennie!"
            army_size = int(params[3])
            attacking_user = await self.chatvilization.get_attackable_chatuser(params[2], chatuser)
            # if attacking_user is a string, then it's an error message
            if isinstance(attacking_user, str):
                return f"Kedves @{chatuser.visible_name}, " + attacking_user
            return await self.chatvilization.attack(chatuser, attacking_user, army_size)
        elif params[1] in ["árelőzmény", "pricehistory"]:
            price_history = await self.chatvilization.get_latest_price(24)
            response = f"Kedves @{chatuser.visible_name}, a katonák legutóbbi árai:\n```log\n"
            if len(params) == 2:
                for price in price_history:
                    response += f"{price.time.strftime('%Y-%m-%d %H:%M')} : {format(price.price, '.2f')} \n"
                response += "```"
                return response
            if params[2] in ["chart", "grafikon"]:
                relative_prices = []
                for price in price_history:
                    relative_prices.append(price.price * 4)
                for row_index in range(20, 0, -1):
                    response += f"{format(row_index / 4, '.2f') }  |"
                    for column_index in range(len(relative_prices)):
                        price = relative_prices[column_index]

                        if math.fabs(price - row_index) < 0.5:
                            response += "● "
                        else:
                            if column_index != len(relative_prices) - 1:
                                next_price = relative_prices[column_index + 1]
                                if price <= row_index and next_price >= row_index:
                                    response += " |"
                                elif price >= row_index and next_price <= row_index:
                                    response += " |"
                                else:
                                    response += "  "
                    response += "\n"
                response += "       " + "--" * len(relative_prices) + "\n"
                response += "       "
                for column_index in range(len(price_history)):
                    if (column_index % 4) == 0:
                        price = price_history[column_index]
                        response += f"|{price.time.strftime('%H:%M')}  "
                response += "```\n"
                return response
        elif params[1] in ["help", "segítség", "-h", "-?"]:
            response = f"Kedves @{chatuser.visible_name}, a Chatvilization játék parancsai:\n"
            response += "```log\n"
            response += "!cv vásárol [mennyiség] - Katonák vásárlása\n"
            response += "!cv elad [mennyiség] - Katonák eladása\n"
            response += "!cv felajánl [mennyiség] [ár] [@felhasználó] - Katonák felajánlása megvásárlásra \n"
            response += "!cv elfogad [ajánlat sorszáma] - Katonák elfogadása\n"
            response += "!cv ajánlatok - Jelenlegi ajánlatok listázása\n"
            response += "!cv állapot - Játék állapotának lekérdezése\n"
            response += "!cv támadás [@felhasználó] [méret] - Támadás indítása\n"
            response += "!cv árelőzmény - Árak lekérdezése\n"
            response += "!cv árelőzmény grafikon - Árak grafikus megjelenítése\n"
            response += "!cv segítség - Segítség kérése (most éppen ezt látod)\n"
            response += "```"
            return response
        return f"Kedves @{chatuser.visible_name}, a parancs nem értelmezhető, kérlek használd a !cv segítség parancsot a lehetséges parancsok megtekintéséhez."
