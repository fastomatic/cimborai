import datetime
from chatengine.games.chatgame import ChatGame
from asgiref.sync import sync_to_async


class Limbo(ChatGame):

    STATE_WAITING = "waiting"
    STATE_COLLECTING_BETS = "collecting_bets"
    STATE_MAKE_MOVES = "make_moves"
    STATE_FORCE_CLOSE = "force_close"

    def __init__(self, chatbot):
        super().__init__(chatbot)
        self.players = {}
        self.game_state = self.STATE_WAITING
        self.game_started = datetime.datetime.now().timestamp()
        self.high_limit = 0
        self.current_total_bets = 0
        self.initialize_game()

    def initialize_game(self):
        self.players = {}
        self.game_state = self.STATE_WAITING
        self.current_total_bets = 0

    def add_player_to_game(self, chatuser, player_bet):
        if not chatuser.username in self.players:
            self.players[chatuser.username] = chatuser
            self.current_total_bets += player_bet
            self.players[chatuser.username].bet = player_bet
            self.players[chatuser.username].guess = 0
        else:
            self.current_total_bets -= self.players[chatuser.username].bet
            self.players[chatuser.username].bet = player_bet
            self.current_total_bets += player_bet

    def get_bet_from_params(self, params, chatuser):
        bet = 0
        response = ""
        if len(params) != 2:
            response = f"Kedves @{chatuser.visible_name}, a játékhoz add meg a !limbo <tét> parancsot."
        elif not params[1].isdigit():
            response = f"Kedves @{chatuser.visible_name}, a tét csak szám lehet."
        else:
            bet = int(params[1])
            if bet < 1:
                response = f"Kedves @{chatuser.visible_name}, a tét csak pozitív egész szám lehet."
                bet = 0
            elif not chatuser.username in self.players:
                response = f"Kedves @{chatuser.visible_name}, sajnos ebből a körből most kimaradtál, de várunk szeretettel a következőben!"
            elif bet > self.players[chatuser.username].current_score:
                response = f"Kedves @{chatuser.visible_name}, sajnos nincs ennyi pontod."
                bet = 0
        return bet, response

    async def settlement(self, is_forced = False):
        response = ""
        log_context = ""
        if is_forced:
            players_without_guess = 0
            log_context += "forced settlement. "
        else:
            # how many players didn't make a guess?
            players_without_guess = sum(
                [1 for player in self.players.values() if player.guess == 0]
            )
        if players_without_guess > 1:
            response += f"Még {players_without_guess} játékos tippjére várunk."
            log_context += f"waiting for players: {', '.join([player.username for player in self.players.values() if player.guess == 0])}."
        elif players_without_guess == 1:
            player_without_guess = [
                player for player in self.players.values() if player.guess == 0
            ][0]
            response += f"Még {player_without_guess.visible_name} tippjére várunk. "
            log_context += f"waiting for player: {player_without_guess.username}."
        else:
            log_context += "evaluating results.\n"
            numbers_guessed = {}
            for player in self.players.values():
                log_context += f"{player.username}: {player.guess}, "
                if player.guess == 0:
                    log_context += "no guess.\n"
                    continue
                if player.guess in numbers_guessed:
                    numbers_guessed[player.guess].append(player)
                    log_context += f"added to {player.guess}, number of players: {len(numbers_guessed[player.guess])}.\n"
                else:
                    numbers_guessed[player.guess] = [player]
                    log_context += f"new number: {player.guess}.\n"
            # which is the lowest number that has only 1 players?
            winner = None
            bet_str = ""
            minimum_guess = max([guess for guess in numbers_guessed.keys()]) + 1
            for guessed_numbers in numbers_guessed.keys():  # pylint: disable=consider-using-dict-items
                player_list = numbers_guessed[guessed_numbers]
                bet_str += f"{guessed_numbers}: ({len(player_list)}), "
                if len(player_list) == 1 and guessed_numbers < minimum_guess:
                    minimum_guess = guessed_numbers
                    winner = player_list[0]
            if winner:
                log_context += f"winner: {winner.username}, number: {winner.guess}\n"
                response += (
                    f"A nyertes: @{winner.visible_name}! A tippje: {winner.guess}."
                )
                total_bets = 0
                for player in self.players.values():
                    if player.username != winner.username:
                        player.current_score -= player.bet
                        total_bets += player.bet
                        await sync_to_async(player.save)()
                        log_context += f"{player.username} lost {player.bet} points, total bets: {total_bets}.\n"
                log_context += f"{winner.username} won {total_bets} points, had {winner.current_score} before.\n"
                winner.current_score += total_bets
                await sync_to_async(winner.save)()
            else:
                response += f"Nem volt nyertes, minden számra több tipp is érkezett. Senki nem veszítette el a tétjét. A tippek: {bet_str}"
                log_context += "no winner, multiple guesses for each number.\n"
            self.initialize_game()
        return response, log_context

    async def player_move(self, chatuser, move, log, is_whisper=False):
        params = move.split(" ")
        response = ""
        log_context = ""
        if (
            self.game_state == self.STATE_COLLECTING_BETS
            and (datetime.datetime.now().timestamp() - self.game_started) > 60
        ):
            self.game_state = self.STATE_MAKE_MOVES
            self.high_limit = len(self.players) * 2
            self.game_started = datetime.datetime.now().timestamp()
            log_context += f"switching to MAKE_MOVES state, timer expired, high limit: {self.high_limit}, total bets: {self.current_total_bets}."
            response = f"A tétfelvétel lezárult, a játék elkezdődött! Adjátok le tippjeiteket SUTTOGÓ ÜZENETBEN 1 és {self.high_limit} között! "
        elif self.game_state == self.STATE_MAKE_MOVES and datetime.datetime.now().timestamp() - self.game_started > 60:
            response += "A tippelési idő lejárt, mutatom az eredményt! "
            log_context += "switching to FORCE_CLOSE state, timer expired."
            self.game_state = self.STATE_FORCE_CLOSE
        if self.game_state == self.STATE_WAITING:
            bet, bet_response = self.get_bet_from_params(params, chatuser)
            if bet == 0:
                response = bet_response
                log_context += "bet not valid."
            else:
                player_bet = int(params[1])
                self.add_player_to_game(chatuser, player_bet)
                response = f"FIGYELEM, egyszámjáték kezdődik! @{chatuser.visible_name} már be is szállt, {player_bet} ponttal."
                self.game_state = self.STATE_COLLECTING_BETS
                log_context += f"switching to COLLECTING_BETS state, {chatuser.visible_name} started game, current score: {chatuser.current_score}."
                self.game_started = datetime.datetime.now().timestamp()
        elif self.game_state == self.STATE_COLLECTING_BETS:
            bet, bet_response = self.get_bet_from_params(params, chatuser)
            if bet == 0:
                response = bet_response
                log_context += "bet not valid."
            else:
                player_bet = int(params[1])
                self.add_player_to_game(chatuser, player_bet)
                response = f"@{chatuser.visible_name}, beszálltál az egyszámjátékba, {player_bet} ponttal. Eddigi tét: {self.current_total_bets} pont."
                log_context += f"bet added to the game. current score: {chatuser.current_score}, total bet: {self.current_total_bets}"
        elif self.game_state == self.STATE_MAKE_MOVES:
            if len(params) != 2:
                response += f"Kedves @{chatuser.visible_name}, a játékhoz add meg a !limbo <tipp> parancsot SUTTOGÓ üzenetben."
            elif not params[1].isdigit():
                response += f"Kedves @{chatuser.visible_name}, a tipp csak szám lehet."
            elif not chatuser.username in self.players:
                response += f"Kedves @{chatuser.visible_name}, sajnos ebből a körből most kimaradtál, de várunk szeretettel a következőben! "
            elif not is_whisper:
                response += f"Kedves @{chatuser.visible_name}, a tipp csak SUTTOGÓ üzenetben adható le."
            else:
                player_guess = int(params[1])
                if player_guess < 1:
                    response += f"Kedves @{chatuser.visible_name}, a tipp csak pozitív egész szám lehet."
                elif player_guess > self.high_limit:
                    response += f"Kedves @{chatuser.visible_name}, a tipp csak 1 és {self.high_limit} közötti szám lehet."
                else:
                    self.players[chatuser.username].guess = player_guess
                    response += (
                        f"Kedves @{chatuser.visible_name}, a tippedet feljegyeztem! "
                    )
                    log_context += f"player {chatuser.username} made a guess: {player_guess}"
                    settlement_result, settlement_log = await self.settlement()
                    response += settlement_result
                    log_context += settlement_log
        elif self.game_state == self.STATE_FORCE_CLOSE:
            settlement_result, settlement_log = await self.settlement(is_forced = True)
            response += settlement_result
            log_context += settlement_log
        log.context = log_context
        await sync_to_async(log.save)()
        return response
