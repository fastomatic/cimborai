from django.db import models
from django.contrib.auth.models import User


class ChatBot(models.Model):
    owner_user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="chatbots", blank=True, null=True
    )
    access_token = models.CharField(max_length=200, blank=True, null=True)
    refresh_token = models.CharField(max_length=200, blank=True, null=True)
    expires_in = models.IntegerField(default=0)
    token_type = models.CharField(max_length=200, blank=True, null=True)
    nickname = models.CharField(max_length=200)
    twitch_channel = models.CharField(max_length=200, blank=True, null=True)
    openai_api_key = models.CharField(max_length=200, blank=True, null=True)
    rephrase_prompt = models.TextField(blank=True, null=True)
    twitch_prefix = models.CharField(max_length=20, blank=True, null=True)
    generic_prompt = models.TextField(blank=True, null=True)
    discord_bot_id = models.CharField(max_length=200, blank=True, null=True)
    discord_access_token = models.CharField(max_length=200, blank=True, null=True)
    discord_default_channel_id = models.CharField(max_length=200, blank=True, null=True)
    discord_celebration_channel_id = models.CharField(max_length=200, blank=True, null=True)
    roulette_dconly = models.BooleanField(default=True)
    blackjack_dconly = models.BooleanField(default=True)
    chatvilization_dconly = models.BooleanField(default=True)
    giveaway_dconly = models.BooleanField(default=True)
    lottery_dconly = models.BooleanField(default=True)
    chatvilization_enabled = models.BooleanField(default=True)
    limit_time_window = models.IntegerField(default=10)
    limit_message_count = models.IntegerField(default=20)
    birthday_greeting = models.TextField(blank=True, null=True)
    nameday_greeting = models.TextField(blank=True, null=True)
    greeting_interval = models.IntegerField(default=3600)
    annotation_whitelist = models.TextField(blank=True, null=True)
    chatvilization_price_change_interval = models.IntegerField(default=60)
    chatvilization_min_price = models.FloatField(default=1.0)
    chatvilization_max_price = models.FloatField(default=5.0)
    chatvilization_rounding_precision = models.IntegerField(default=5)
    chatvilization_min_percent = models.FloatField(default=2.0)
    chatvilization_max_percent = models.FloatField(default=5.0)

    def __str__(self):
        return f"{self.nickname} ({self.twitch_channel})"


class ChatUser(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    platform = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    callname = models.CharField(max_length=200, blank=True, null=True)
    current_score = models.IntegerField(default=1000)
    twitch_sub_tier = models.IntegerField(default=0, blank=True, null=True)
    connect_code = models.CharField(max_length=200, blank=True, null=True)
    parent_user = models.ForeignKey(
        "chatengine.ChatUser", on_delete=models.DO_NOTHING, blank=True, null=True
    )
    can_teach = models.BooleanField(default=True)
    current_knowledge = models.ForeignKey(
        "chatengine.KnowledgeItems",
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
    )
    can_close_giveaway = models.BooleanField(default=False)
    can_play_music = models.BooleanField(default=False)
    birthday = models.DateField(blank=True, null=True)
    nameday = models.DateField(blank=True, null=True)

    @property
    def visible_name(self):
        return self.callname or self.username

    def __str__(self):
        return f"{self.username} ({self.platform})"

    class Meta:
        unique_together = ("chatbot", "platform", "username")


class SimpleCommands(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    command = models.CharField(max_length=100, unique=True)
    response = models.TextField()
    is_verbatim = models.BooleanField(default=False)
    is_containable = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.command} -> {self.response}"


class KnowledgeItems(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    question = models.TextField()
    answer = models.TextField()
    is_approved = models.BooleanField(default=False)
    from_chatuser = models.ForeignKey(
        ChatUser, on_delete=models.DO_NOTHING, blank=True, null=True
    )

    def __str__(self):
        return f"{self.question} -> {self.answer}"


class ChatLog(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    platform = models.CharField(max_length=200)
    chatuser = models.ForeignKey(
        ChatUser, on_delete=models.CASCADE, blank=True, null=True
    )
    sender = models.CharField(max_length=200)
    message = models.TextField()
    response = models.TextField(blank=True, null=True)
    context = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    debug_context = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.sender} ({self.created_at}/{self.platform})"

    class Meta:
        ordering = ["-created_at"]


class RandomGiveaway(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    description = models.TextField()
    start_at = models.DateTimeField()
    end_at = models.DateTimeField()
    participants = models.ManyToManyField(ChatUser, blank=True)
    winner = models.ForeignKey(
        ChatUser,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="winner",
    )

    def __str__(self):
        return f"{self.name} ({self.chatbot})"


class ScheduledMessage(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    platform = models.CharField(max_length=200, null=True, blank=True)
    message = models.TextField()
    start_at = models.DateTimeField(null=True, blank=True)
    end_at = models.DateTimeField(null=True, blank=True)
    interval = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.message} ({self.chatbot})"


class LotteryTicket(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    chatuser = models.ForeignKey(ChatUser, on_delete=models.CASCADE)
    guesses = models.JSONField(default=dict)
    bet_amount = models.IntegerField(default=0)
    is_settled = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.chatuser} ({self.guesses})"


class NoResponseExcuses(models.Model):
    chatbot = models.ForeignKey(ChatBot, on_delete=models.CASCADE)
    excuse = models.TextField()

    def __str__(self):
        return f"{self.excuse} ({self.chatbot})"
