from asgiref.sync import sync_to_async
from django.utils import timezone
from django.template import Template, Context
from chatengine.models import ScheduledMessage, ChatUser, ChatLog


class BotScheduler:
    def __init__(self):
        self.message_log = {}
        self.callback_tasks = []
        self.birthday_celebration_timestamps = {}
        self.nameday_celebration_timestamps = {}
        self.celebration_timestamp = timezone.now()

    def add_task(self, task):
        if not task:
            print("ERROR: add_task failed, task is None")
            return
        self.callback_tasks.append(task)

    async def get_task_messages(self, log, platform):
        messages = []
        for task in self.callback_tasks:
            task_message = await task(log, platform)
            if task_message:
                messages.append(task_message)
        return messages

    @sync_to_async
    def get_celebration_messages(self, platform, chatengine):
        if (
            timezone.now() - self.celebration_timestamp
        ).seconds < (chatengine.chatbot.greeting_interval // 10):
            return []
        self.celebration_timestamp = timezone.now()
        messages = []
        for chatuser in ChatUser.objects.filter(
            chatbot=chatengine.chatbot,
            platform=platform,
            birthday__isnull=False,
            birthday__day=timezone.now().day,
            birthday__month=timezone.now().month,
        ):
            if not chatengine.is_user_present(chatuser.id, platform):
                continue
            if chatuser.id not in self.birthday_celebration_timestamps:
                self.birthday_celebration_timestamps[chatuser.id] = timezone.now()
            else:
                if (
                    timezone.now() - self.birthday_celebration_timestamps[chatuser.id]
                ).seconds >= chatengine.chatbot.greeting_interval:
                    if chatengine.chatbot.birthday_greeting:
                        greeting_template = Template(chatengine.chatbot.birthday_greeting)
                        greeting = greeting_template.render(
                            Context({"chatuser": chatuser.visible_name, "chatengine": chatengine})
                        )
                        greeting = chatengine.rephrase_text(greeting)
                        messages.append(greeting)
                        self.birthday_celebration_timestamps[chatuser.id] = timezone.now()
        for chatuser in ChatUser.objects.filter(
            chatbot=chatengine.chatbot,
            platform=platform,
            nameday__isnull=False,
            nameday__day=timezone.now().day,
            nameday__month=timezone.now().month,
        ):
            if not chatengine.is_user_present(chatuser.id, platform):
                continue
            if chatuser.id not in self.nameday_celebration_timestamps:
                self.nameday_celebration_timestamps[chatuser.id] = timezone.now()
            else:
                if (
                    timezone.now() - self.nameday_celebration_timestamps[chatuser.id]
                ).seconds >= chatengine.chatbot.greeting_interval:
                    if chatengine.chatbot.nameday_greeting:
                        greeting_template = Template(chatengine.chatbot.nameday_greeting)
                        greeting = greeting_template.render(
                            Context({"chatuser": chatuser.visible_name, "chatengine": chatengine})
                        )
                        greeting = chatengine.rephrase_text(greeting)
                        messages.append(greeting)
                        self.nameday_celebration_timestamps[chatuser.id] = timezone.now()
        return messages

    @sync_to_async
    def get_periodic_messages(
        self, platform, chatengine
    ):  # pylint: disable=unused-argument
        # We need to iterate on the ScheduledMessage objects that EITHER don't have start_at or end_at set,
        # or we are between start_at and end_at.
        response = []
        qs = ScheduledMessage.objects.filter(
            start_at__isnull=True, end_at__isnull=True, is_active=True, chatbot=chatengine.chatbot
        )
        qs |= ScheduledMessage.objects.filter(
            start_at__lte=timezone.now(), end_at__gte=timezone.now(), is_active=True, chatbot=chatengine.chatbot
        )
        for msg in qs:
            if not msg.platform or msg.platform == platform:
                if msg.id not in self.message_log:
                    self.message_log[msg.id] = {"last_sent": timezone.now()}
                    response.append(msg.message)
                else:
                    if (
                        timezone.now() - self.message_log[msg.id]["last_sent"]
                    ).seconds >= msg.interval:
                        response.append(msg.message)
                        self.message_log[msg.id]["last_sent"] = timezone.now()
        return response


bot_scheduler = BotScheduler()


def get_scheduler():
    return bot_scheduler


async def run_tasks(platform, chatengine):
    messages = await bot_scheduler.get_periodic_messages(platform, chatengine)
    messages += await bot_scheduler.get_celebration_messages(platform, chatengine)
    # We need to append the messages coming from tasks
    log = ChatLog(
        chatbot=chatengine.chatbot,
        platform=platform,
        sender="scheduler",
        message="Running tasks",
        context="",
    )
    messages += await bot_scheduler.get_task_messages(log, platform)
    log.response = "\n".join(messages)
    if log.response:
        await sync_to_async(log.save)()
    return messages
